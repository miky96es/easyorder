Notas sobre la base de datos

=========== Roles de USUARIO =========

1. Admin
2. Cocinero
3. Camarero

========= Estado de MESA =========

1. Desocupada
2. Ocupada

======== Tipo de Producto ========

1. Primer plato
2. Segundo plato
3. Entrante
4. Bebida
5. Postre

======== Estado Producto =======

1. NO PREPARADO nada
2. EN PREPARACION celeste
3. PREPARADO verde
4. ENTREGADO gris
5. CANCELADO rojo

======== Estado pago de Pedido =======

1. NO PAGADO
2. PAGADO