/*=================== INSERT USUARIO MIGUEL Y MARCO (ADMINS) =====================================*/

INSERT INTO USUARIO VALUES ('47865463W', 'Cesar Miguel', 'Catalán Raneros', '640187424', 1, '1234',
                                         'cesar.catalan@iesjoandaustria.org', '1996/03/02', 'ESPAÑOLA', 3000.00,
                                         '47865463W.jpg');

INSERT INTO USUARIO VALUES ('49811894N', 'Marco Ismael', 'Realpe Cobos', '675469752', 1, '1234',
                                         'marco.realpe@iesjoandaustria.org', '1988/12/11', 'ESPAÑOLA', 4000.12,
                                         DEFAULT );

INSERT INTO USUARIO VALUES ('X3576765N', 'Kleber Humberto', 'Realpe', '675469753', 1, 'miguel',
                                         'kleber.realpe@iesjoandaustria.org', '1968/12/11', 'ESPAÑOLA', 3000.12,
                                         DEFAULT);

INSERT INTO USUARIO VALUES ('49811892Z', 'Jacqueline', 'Cobos Játiva', '669087636', 3, '1234',
                                         'jacqueline.cobos@iesjoandaustria.org', '1968/12/11', 'ESPAÑOLA', 3000.12,
                                         DEFAULT);

INSERT INTO USUARIO VALUES ('12345678A', 'Adrià','Tomas', '689645259', 2, '1234',
                                         'adria.tomas@iesjoandaustria.org', '1994/10/24', 'ESPAÑOLA', 3000.12,
                                         DEFAULT);

/*============================ INSERTS MESAS =======================================*/
/*ESTADO MESA: 1: DESOCUPADA, 2: OCUPADA, 3: RESERVADA*/

/*MESA ID DEFAULT, ESTADO: DESOCUPADA, NUM_COMENSALES: 6, ID_RESERVA: NULL (SIN RESERVA) */
INSERT INTO MESA VALUES (DEFAULT, 1, 6);

/*MESA ID DEFAULT, ESTADO: DESOCUPADA, NUM_COMENSALES: 2, ID_RESERVA: NULL (SIN RESERVA) */
INSERT INTO MESA VALUES (DEFAULT, 1, 2);

/*MESA ID DEFAULT, ESTADO: DESOCUPADA, NUM_COMENSALES: 4, ID_RESERVA: NULL (SIN RESERVA) */
INSERT INTO MESA VALUES (DEFAULT, 1, 4);

/*MESA ID DEFAULT, ESTADO: DESOCUPADA, NUM_COMENSALES: 10, ID_RESERVA: NULL (SIN RESERVA) */
INSERT INTO MESA VALUES (DEFAULT, 1, 10);

/*==============================  INSERTS TIPO PRODUCTO ==============================*/
/*TIPOS: 1: PRIMER PLATO, 2: SEGUNDO PLATO, 3: ENTRANTE, 4: BEBIDA, 5: POSTRE */

/*PRIMER PLATO*/
INSERT INTO TIPO_PRODUCTO VALUES (DEFAULT, 'Primer plato');

/*SEGUNDO PLATO*/
INSERT INTO TIPO_PRODUCTO VALUES (DEFAULT, 'Segundo plato');

/*ENTRANTE*/
INSERT INTO TIPO_PRODUCTO VALUES (DEFAULT, 'Entrante');

/*BEBIDA*/
INSERT INTO TIPO_PRODUCTO VALUES (DEFAULT, 'Bebida');

/*POSTRE*/
INSERT INTO TIPO_PRODUCTO VALUES (DEFAULT, 'Postre');


/*========================= INSERTS SUBTIPO_PRODUCTO ========================*/

/*Subtipos de PRIMER PLATO*/

INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Ensalada', 1, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Sopa', 1, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Crema', 1, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Verduras', 1, NULL);


/*Subtipos de SEGUNDO PLATO*/

INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Pasta', 2, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Arroz', 2, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Pizza', 2, NULL);


/*Subtipos de ENTRANTES*/

INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Buñuelos', 3, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Patatas', 3, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Croquetas', 3, NULL);

/*Subtipos de BEBIDAS*/

INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Agua', 4, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Refresco', 4, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Alcohólica', 4, NULL);


/*Subtipos de POSTRES*/

INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Fruta', 5, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Helado', 5, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Tarta', 5, NULL);
INSERT INTO SUBTIPO_PRODUCTO VALUES (DEFAULT, 'Yogur', 5, NULL);


/*========================= INSERTS PRODUCTO ========================*/

/*PRIMER PLATO */

INSERT INTO PRODUCTO VALUES(DEFAULT,'Ensalada 4  estaciones',
'Una armoniosa mezcla que combina la frescura de la lechuga iceberg,el color morado de la col lombarda y el sabor dulce de la zanahoria.',
12.50,1,'048-salad.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Sopa Juliana',
'Sopa de verduras con tomate, ajo, pimentón dulce, apio, cebolla y perejil.',
12.50, 2, '049-soup.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Crema de calabacín',
'Crema de calabaza con picatostes de pan tostado.', 13.50, 3, '050-cream.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Menestra de verduras',
'Menestra de  verduras con guisantes, zanahoria, puerro, alcachofa y judías verdes.', 13.50, 4, '051-vegetables.png');


/*SEGUNDO PLATO*/

INSERT INTO PRODUCTO VALUES(DEFAULT,'Pasta con alcachofas y requesón',
'Pasta gruesa con alcachofas, requesón dientes de ajo, y vino blanco',
16.50,5,'052-pasta.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Arroz con verduras',
'Arroz con calabacín, alcachofas, pimiento, coliflor,ajos, tomates y aceite de oliva.',
14.50, 6, '053-rice.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Pizza barbacoa familiar',
'Pizza con queso mozarella, bacon, ternera, pollo y salsa barbacoa.',
18.50, 7, '055-pizza.png');

/*ENTRANTES*/

INSERT INTO PRODUCTO VALUES(DEFAULT,'Buñuelos de atún',
'Buñuelo con atún, pimienta negra y toque de aceite de oliva.',
6.50,8,'054-flut.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Patatas bravas',
'Patatas fritas en aceite de oliva, acompañadas de salsa brava(cebolla, ajos, pimentón, tomate natural triturado y tomate frito).',
5.50, 9,'056-papas.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Croquetas de gambas',
'Croquetas con gambas arroceras, mantequilla, cebolla y concentrado de gambas.',
8.50, 10,'057-croqueta.png');

/*BEBIDAS*/

INSERT INTO PRODUCTO VALUES(DEFAULT,'Agua 2l',
'Botella de agua fontvella de 2 litros.',
2.50,11,'058-water.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Cocacola 355ml',
'Lata de cocacola normal 355ml .',
2.50, 12, '12.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Fanta naranja 355ml',
'Lata de fanta naranja normal 355ml .',
2.50, 12, '13.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Vino tinto Ribera del Duero',
'Botella de vino tinto Carmelo Rodero 9 meses.',
16.50, 13, '059-wine.png');


/*POSTRES*/
INSERT INTO PRODUCTO VALUES(DEFAULT,'MACEDONIA',
'Ensalada de frutas.',
4.50,14,'060-fruit-salad.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Helado de fresa',
'Helado de crema de fresa.',
4.50, 15, '061-ice-cream.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Tarta de queso',
'Tarta de queso con arándanos.',
4.50, 16, '062-cake.png');

INSERT INTO PRODUCTO VALUES(DEFAULT,'Yogur de coco',
'Yogur de coco una unidad 125g.',
16.50, 17, '063-yogurt.png');

/*PEDIDOS*/

/*NOTA: CUANDO SE HACE UN PEDIDO HAY QUE CAMBIAR EL ESTADO DE LA MESA*/
/*ID,FECHA,MESA,ESTADO_PAGO*/

/*INSERT INTO PEDIDO VALUES (DEFAULT, DEFAULT, DEFAULT, 1, '49811894N');*/


/*LINEA DE PEDIDO*/
/*ID, ID_PEDIDO, ID_MENU, ID_PRODUCTO,ESTADO_PRODUCTO*/
/*pide una ensalada 4 estaciones*/
/*INSERT INTO LINEA_PEDIDO VALUES (DEFAULT, 1, DEFAULT, 1, DEFAULT);*/


/*MENU DE EJEMPLO
creación de un menu de ejemplo*/

/*Id, nombre descripción, precio, foto*/
INSERT INTO MENU VALUES (DEFAULT, "Menú de ejemplo", "Esto es un menú de prueba", 12.50, DEFAULT);

/*id_menu, id_producto*/
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 1);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 4);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 5);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 7);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 8);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 10);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 11);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 12);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 13);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 17);
INSERT INTO MENU_COMPONE_PRODUCTO VALUES (1, 18);


/*MENU*/
/*ID, NOMBRE, DESCRIPCION, PRECIO, URL_FOTO_MENU */
INSERT INTO MENU VALUES (DEFAULT, 'Carnívoro',
                         'Destinado a aquellas personas amantes de la carne, con una gran selección de carnes de calidad a escoger.',
                         20, NULL);