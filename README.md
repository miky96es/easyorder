# EASY ORDER #

## Que es EasyOrder? ##
* Easy order es un aplicación orientada a la restauración, con ella se podrá gestionar un negocio relacionado con la venta de comida.
 
## Pre-requisitos ##
* Un S.O. Linux con Apache, Mysql y PHP. (Recomendamos usar el gestor de paquetes apt-get), en caso de no tenerlos usaremos el siguiente comando.

```
#!shell

  $  sudo apt-get install apache2 mysql-server php7.0 libapache2-mod-php7.0 php7.0-mysql php7.0-curl php7.0-json php7.0-mcrypt php7.0-soap

```

## Instalación de EasyOrder ##
* Una vez tenemos el servidor correctamente configurado seguiremos las siguientes instrucciones:
CUANDO LO TENGAMOS SE EXPLICARA

## Para desarrolladores ##
Para empezar a desarrollar la aplicación debemos cumplir los pre-requisitos y posteriormente instalar git.
Para instalar git debemos hacer: 

```
#!shell

$ sudo apt-get install git

```
Una vez hecho clonaremos nuestro proyecto de bitbucket que es donde se encuentra. 
Para ello nos ubicamos en la carpeta en donde se guardará nuestro repositorio local y clonamos el repositorio y lo configuramos: 

Ejemplo:

* Crear carpeta donde se guarda el repositorio
```
#!shell

$ mkdir ~/easyorder

```
* Ir a la carpeta recin creada
```
#!shell

$ cd ~/easyorder

```
* Clonar el repositorio en la propia carpeta creada anteriormente y donde estamos ubicados actualmente si hemos seguido bien los pasos
```
#!shell

$ git clone https://miky96es@bitbucket.org/miky96esyorder.git .

```
* Poner en modo track la rama master 
```
#!shell

$ git branch --track origin/master
```
* Configurar el usuario que subirá al repositorio remoto

```
#!shell

$ git config --global user.name "Cesar Miguel Catalan Raneros"
$ git config --global user.email "cesar.catalan@iesjoandaustria.org"
```


### Chivatos de Git ###

* Estado del git
```
#!shell

$ git status
```
* Añadir fichero al repositorio

```
#!shell

$ git add ficheroejemplo.txt
```

* Hacer un commit 
```
#!shell

$ git commit -m "Mensaje de ejemplo"
```
* Hacer un pull
```
#!shell

$ git pull
```
* Hacer un push
```
#!shell

$ git push
```