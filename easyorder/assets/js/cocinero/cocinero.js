$(function(){
	function main(){
		listado_cola();
		listado_listos();
	}

	setInterval(main,5000);

	function listado_cola(){
		$.get('../../c_cocinero/ajax_get_listado_cola', function( data ) {
			var res = jQuery.parseJSON(data)

			var tipoA = '<tr data-status="{{estado_producto}}">'+
			  '<td>{{fecha}}</td>'+
			  '<td>{{nombre}}</td>'+
			  '<td width="200px">'+
			    '{{descripcion}}'+
			  '</td>'+
			  '<td>{{estado}}</td>'+
			  '<td>'+
			    '<div class="btn-group">'+
			        '<button class="btn btn-success btn-sm btn-move" data-id="{{id}}">Mover a Preparación</button>'+
			    '</div>'+
			  '</td>'+
			'</tr>'

			var tipoB = '<tr data-status="{{estado_producto}}">'+
			  '<td>{{fecha}}</td>'+
			  '<td>{{nombre}}</td>'+
			  '<td width="200px">'+
			    '{{descripcion}}'+
			  '</td>'+
			  '<td>{{estado}}</td>'+
			  '<td>'+
			    '<div class="btn-group">'+
							'<button class="btn btn-success btn-sm btn-ready" data-id="{{id}}">Listo</button>'+
							'<button class="btn btn-warning btn-sm btn-return" data-id="{{id}}">Devolver a Cola</button>'+
			    '</div>'+
			  '</td>'+
			'</tr>'

			var row = "";
			$.each(res, function(i, item) {
					if(item.estado_producto == 1){
						row += tipoA
							.replace('{{estado_producto}}',item.estado_producto)
							.replace('{{fecha}}',item.fecha)
							.replace('{{nombre}}',item.nombre)
							.replace('{{descripcion}}',item.descripcion)
							.replace('{{estado}}',item.estado_producto == 1 ? "En Cola" : "En Preparación")
							.replace('{{id}}',item.id)
					}else{
						row += tipoB
							.replace('{{estado_producto}}',item.estado_producto)
							.replace('{{fecha}}',item.fecha)
							.replace('{{nombre}}',item.nombre)
							.replace('{{descripcion}}',item.descripcion)
							.replace('{{estado}}',item.estado_producto == 1 ? "En Cola" : "En Preparación")
							.replace(/{{id}}/g,item.id)
					}
			});
		  $( ".table.cola > tbody" ).html( row );
		});
	}

	function listado_listos(){
		$.get('../../c_cocinero/ajax_get_listado_listos', function( data ) {
			var res = jQuery.parseJSON(data)

			var base = '<tr data-status="{{estado_producto}}">'+
			  '<td>{{fecha}}</td>'+
			  '<td>{{nombre}}</td>'+
			  '<td>#{{id_pedido}}</td>'+
			  '<td>'+
			    '<div class="btn-group">'+
			        '<button class="btn btn-danger btn-sm btn-delivered" data-id="{{id}}">Liberar</button>'+
							'<button class="btn btn-info btn-sm btn-return" data-id="{{id}}">Devolver a Cola</button>'+
			    '</div>'+
			  '</td>'+
			'</tr>'

			var row = "";
			$.each(res, function(i, item) {
						row += base
							.replace('{{estado_producto}}',item.estado_producto)
							.replace('{{fecha}}',item.fecha)
							.replace('{{nombre}}',item.nombre)
							.replace('{{id_pedido}}',item.id_pedido)
							.replace(/{{id}}/g,item.id)
			});
		  $( ".table.listos > tbody" ).html( row );
		});
	}

	$(document.body).on('click', ".btn-move", function(){
		var row = $(this).parent().parent().parent();
		var id_pedido = $(this).data('id');
		$.post('../../c_cocinero/set_estado/' + id_pedido, {to: 2},
		    function(data)
		    {
					var res = jQuery.parseJSON(data)
					if(res.estado == 1){
						listado_cola();
					}else{
						alert("error");
					}
		    }
	);

	});

	$(document.body).on('click', ".btn-ready", function(){
		var row = $(this).parent().parent().parent();
		var id_pedido = $(this).data('id');
		$.post('../../c_cocinero/set_estado/' + id_pedido, {to: 3},
		    function(data)
		    {
					var res = jQuery.parseJSON(data)
					if(res.estado == 1){
						listado_cola();
						listado_listos();
					}else{
						alert("error");
					}
		    }
	);
	});

	$(document.body).on('click', ".btn-return", function(){
		var row = $(this).parent().parent().parent();
		var id_pedido = $(this).data('id');
		$.post('../../c_cocinero/set_estado/' + id_pedido, {to: 1},
		    function(data)
		    {
					var res = jQuery.parseJSON(data)
					if(res.estado == 1){
						listado_cola();
						listado_listos();
					}else{
						alert("error");
					}
		    }
	);
	});

	$(document.body).on('click', ".btn-delivered", function(){
		var row = $(this).parent().parent().parent();
		var id_pedido = $(this).data('id');
		$.post('../../c_cocinero/set_estado/' + id_pedido, {to: 4},
		    function(data)
		    {
					var res = jQuery.parseJSON(data)
					if(res.estado == 1){
						listado_listos();
					}else{
						alert("error");
					}
		    }
	);
	});
});
