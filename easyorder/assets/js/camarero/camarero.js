/**
 * Created by marco on 15/05/17.
 */
$(document).ready(main);
var mesa_elegida;
function main() {
	/*====== Cuando se selecciona una mesa ========= */
	$(".mesa").click(function () {
		mesa_elegida = $(this).data("id");
		ocultar_titulo();
		ocultar_mesas();
		get_estado_mesa(mesa_elegida);
	});

	$("#switch").click(function () {
		get_estado_mesa($('#id_mesa').val());
	});

	$(".entrantes").click(function () {
		mostrar_tipo_entrantes();
	});

	$(".platos").click(function () {
		mostrar_tipo_1plato();
		mostrar_tipo_2plato()
	});

	$(".bebidas").click(function () {
		mostrar_tipo_bebidas();
	});

	$(".postres").click(function () {
		mostrar_tipo_postres();
	});

	$(".menus").click(function () {
		mostrar_menus();
	});
	$(".btn-enviar-pedido").click(function () {
		registrar_pedido($("#ids-productos").html(),mesa_elegida);

		//$("#detalles-pedido").append("<div>"++" <span class=\" btn glyphicon glyphicon-plus\"></span></div>");
	});
}

function mostrar_nuevo_pedido() {
	$("#nuevo_pedido").css("display", "block");
	mostrar_tipo_entrantes();
	$( "#productos-listado-acordeon" ).accordion({
		collapsible: true,
		heightStyle: "content"
	});
}

function ocultar_mesas() {
	$("#mesas").css("display", "none");
}

function ocultar_titulo() {
	$("#titulo").css("display", "none");
}


/*function mostrar_switch() {
	$("#content_switch").css("display", "inline-block");
}*/

function get_estado_mesa(id_mesa) {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/get_estado_mesa/'+ id_mesa,
		dataType: 'json',
		contentType: 'application/json',
		success: function (response) {
			if (response["estado"] == "1") {
				cambio_estado_ocupada(id_mesa);
				mostrar_nuevo_pedido();
			}
			else {
				$.ajax({
					type: 'POST',
					url: '../../c_camarero/ajax_tiene_pedido/'+ id_mesa,
					dataType: 'json',
					contentType: 'application/json',
					success: function (response) {
						if (response == 0) {
							mostrar_nuevo_pedido();
						}
						else {
							mostrar_pedido_realizado(id_mesa);
						}
					},
					error: function () {
						alert("Hay un error");
					},
				});
			}
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function cambio_estado_ocupada(id_mesa) {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_set_estado_ocupada/'+ id_mesa,
		dataType: 'json',
		contentType: 'application/json',
		success: function (response) {
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function cambio_estado_desocupada(id_mesa) {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_set_estado_desocupada/'+ id_mesa,
		dataType: 'json',
		contentType: 'application/json',
		success: function (response) {
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function mostrar_tipo_entrantes() {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_tipo_producto/3',
		//dataType: 'json',
		//contentType: 'application/json',
		success: function (response) {
			$("#detalle_tipo_producto").html(response);

			/*$('div#detalles-productos-accordion').first-child().html("HOLA");*/

			$("#tipo-producto").html("Entrantes");
			$(".subtipo").click(function () {
				var id = $(this).data("id");
				mostrar_subtipo_productos(id);
			});
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function mostrar_tipo_1plato() {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_tipo_producto/3',
		success: function (response) {
			$("#detalle_entrantes").html(response);
			$("#tipo-producto").html("Platos");
			$(".subtipo").click(function () {
				var id = $(this).data("id");
				mostrar_subtipo_productos(id);
			});
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function mostrar_tipo_2plato() {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_tipo_producto/2',
		success: function (response) {
			$("#detalle_tipo_producto").html(response);
			$("#tipo-producto").html("Platos");
			$(".subtipo").click(function () {
				var id = $(this).data("id");
				mostrar_subtipo_productos(id);
			});
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function mostrar_tipo_bebidas() {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_tipo_producto/4',
		success: function (response) {
			$("#detalle_tipo_producto").html(response);
			$("#tipo-producto").html("Bebidas");
			$(".subtipo").click(function () {
				var id = $(this).data("id");
				mostrar_subtipo_productos(id);
			});
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function mostrar_tipo_postres() {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_tipo_producto/5',
		//dataType: 'json',
		//contentType: 'application/json',
		success: function (response) {
			$("#detalle_tipo_producto").html(response);
			$("#tipo-producto").html("Postres");
			$(".subtipo").click(function () {
				var id = $(this).data("id");
				mostrar_subtipo_productos(id);
			});
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function mostrar_menus() {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_menus',
		//dataType: 'json',
		//contentType: 'application/json',
		success: function (response) {
			$("#detalle_tipo_producto").html(response);
			$("#tipo-producto").html("Menús");
			$(".menu").click(function () {
				var id = $(this).data("id");
				mostrar_menu(id);
			});
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function mostrar_subtipo_productos(id) {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_subtipo_producto/' + id,
		success: function (response) {
			$("#detalle_tipo_producto").html(response);
			$(".detalles-producto").css('display','none');
			$(".subtipo").click(function () {
				var producto = $(this).data("id");
				$(".detalles-producto").css('display','none');
				$('#producto-'+producto).toggle();

			});
			$(".btn-añadir-pedido").click(function () {
				añadir_listado_pedido($(this).data("id"));

				//$("#detalles-pedido").append("<div>"++" <span class=\" btn glyphicon glyphicon-plus\"></span></div>");
			});


		},
		error: function () {
			alert("Hay un error");
		},
	});
}

/*function mostrar_producto(id) {
	$.ajax({
		type: 'POST',
		url: '../../c_producto/ajax_get_producto/' + id,
		success: function (response) {
			$("#detalle_tipo_producto").html(response);
		},
		error: function () {
			alert("Hay un error");
		},
	});
}*/

function mostrar_menu(id) {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_menu/' + id,
		success: function (response) {
			$("#detalle_tipo_producto").html(response);
			mostrar_productos_menu(id);
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function mostrar_productos_menu(id) {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_productos_menu/' + id,
		success: function (response) {
			$("#platos").html(response);
			$(".info-producto-menu").css('display','none');
			$( "#accordion" ).accordion({
				collapsible: true,
				heightStyle: "content"
			});
			$(".producto-menu").click(function () {
				var producto = $(this).data("id");
				$(".info-producto-menu").css('display','none');
				$('#'+producto).toggle();
			});
			$(".btn-añadir-pedido").click(function () {
				añadir_listado_pedido($(this).data("id"));

				//$("#detalles-pedido").append("<div>"++" <span class=\" btn glyphicon glyphicon-plus\"></span></div>");
			});
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function añadir_listado_pedido(id) {
	var ids = ids + $(this).data("id")+",";
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_get_producto_tabla/' + id,
		success: function (response) {
			$("#tabla_listado_productos_pedido").append(response);
			$("#ids-productos").append(id+",");
			$("#ids-productos").css("display","none");

		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function registrar_pedido(ids_productos,mesa){

	//primero se crea el nuevo pedido
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_alta_pedido',
		data: {mesa: mesa, productos: ids_productos},
		success: function (response) {
			if(response == 1){
				$( "#dialog-pedido-ok" ).dialog({
					modal: true,
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
							window.location.replace('../../c_camarero');
						}
					}
				});
			}
			else{
				$( "#dialog-pedido-error" ).dialog({
					modal: true,
					buttons: {
						Ok: function() {
							$( this ).dialog( "close" );
							window.location.replace('../../c_camarero');
						}
					}
				});
			}
		},
		error: function () {
			alert("Hay un error");
		},
	});

}

function mostrar_pedido_realizado(id_mesa){
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_pedido_realizado/'+id_mesa,
		success: function (response) {
			$("#pedido-realizado").css("display","block");
			$("#pedido-realizado").html(response);
			$(".btn-cobrar").click(function () {
				generar_factura(id_mesa);
			});
			$(".btn-add-orden").click(function () {
				$("#pedido-realizado").css("display","none");
				mostrar_nuevo_pedido();
			});

			$(".btn-cancelar-orden").click(function () {
				 cancelar_orden($(this).data("id"));
			});
			$(".btn-entregar-orden").click(function () {
				entregar_orden(mesa_elegida = $(this).data("id"));

			});


		},
		error: function () {
			alert("Hay un error");
		},
	});
}

/*=============== COBROS ===========*/

function generar_factura(id_mesa){
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_generar_factura/'+id_mesa,
		success: function (response) {
			$("#pedido-realizado").css("display","none");
			$("#factura").html(response);
			$(".btn-pagado").click(function () {
				marcar_pedido_pagado(id_mesa);
			})

		},
		error: function () {
			alert("Hay un error");
		},
	});
}

function marcar_pedido_pagado(id_mesa){
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_marcar_pagado/'+id_mesa,
		success: function (response) {
			if (response == 0) {
				//no se ha cambiado a cobrado
			}
			else {
				cambio_estado_desocupada(id_mesa);
				window.location.replace('../../c_camarero');
			}
		},
		error: function () {
			alert("Hay un error");
		},
	});
}

/*=========== ORDENES DE PEDIDO ============*/

function cancelar_orden($id_orden) {
	$( "#dialog-cancelar" ).dialog({
		modal: true,
		buttons: {
			Ok: function() {
				$( this ).dialog( "close" );
				$.ajax({
					type: 'POST',
					url: '../../c_camarero/ajax_cancelar_orden/'+$id_orden,
					success: function (response) {
						mostrar_pedido_realizado(mesa_elegida);
						$( "#dialog-cancelar-ok" ).dialog({
							modal: true,
							buttons: {
								Ok: function() {
									$( this ).dialog( "close" );
								}
							}
						});
					},
					error: function () {
						alert("Hay un error");
					},
				});
			},
			Cancelar: function() {
				$( this ).dialog( "close" );
			}
		}
	});
}

function entregar_orden($id_orden) {
	$.ajax({
		type: 'POST',
		url: '../../c_camarero/ajax_entregar_orden/'+$id_orden,
		success: function (response) {
			location.reload(true);
		},
		error: function () {
			alert("Hay un error");
		},
	});
}








