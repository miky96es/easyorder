$(document).ready(main);
function main() {
	$("#tables-table").tablesorter(
		{
			sortList: [[0, 0]],

			// pass the headers argument and assign a object
			headers: {
				// assign the third column (we start counting zero)
				3: {
					// disable it by setting the property sorter to false
					sorter: false
				},
				// assign the fourth column (we start counting zero)
				4: {
					// disable it by setting the property sorter to false
					sorter: false
				}
			}
		}
	);

	$("#products-table").tablesorter(
		{
			// pass the headers argument and assign a object
			sortList: [[0, 0]],
			headers: {

				3: {
					// disable it by setting the property sorter to false
					sorter: false
				},
				7: {
					// disable it by setting the property sorter to false
					sorter: false
				},
				8: {
					// disable it by setting the property sorter to false
					sorter: false
				},
				1: {
					// disable it by setting the property sorter to false
					sorter: false
				}
			}
		}
	);

	$("#menus-table").tablesorter(
		{
			sortList: [[0, 0]],

			// pass the headers argument and assign a object
			headers: {
				3: {
					// disable it by setting the property sorter to false
					sorter: false
				},
				5: {
					// disable it by setting the property sorter to false
					sorter: false
				},
				6: {
					// disable it by setting the property sorter to false
					sorter: false
				},
				7: {
					// disable it by setting the property sorter to false
					sorter: false
				}
			}
		}
	);

	$("#pedidos-table").tablesorter(

		{
			sortList: [[0, 0]],

			// pass the headers argument and assign a object
			headers: {
				5: {
					// disable it by setting the property sorter to false
					sorter: false
				}
			}
		}
	);
	$("#users-table").tablesorter(
		{
			sortList: [[0, 0]],

			headers: {
				1: {
					sorter: false
				},
				10: {
					sorter: false
				},
				11: {
					sorter: false
				}
			}
		}
	);
	$("#buscador-tables").on(
		'input',
		function () {
			filtrado('#tables-table', '.linea', this.value);
		}
	);

	$("#buscador-users").on(
		'input',
		function () {
			filtrado('#users-table', '.linea', this.value);
		}
	);

	$("#buscador-menus").on(
		'input',
		function () {
			filtrado('#menus-table', '.linea', this.value);
		}
	);

	$("#buscador-products").on(
		'input',
		function () {
			filtrado('#products-table', '.linea', this.value);
		}
	);

	$("#buscador-pedidos").on(
		'input',
		function () {
			filtrado('#pedidos-table', '.linea', this.value);
		}
	);

	filtrado = function (id, sel, filter) {
		var table, trs, interior_tr, contador1, contador2, contador3, existe;//declaracion variables
		table = w3.getElements(id);//coge los elementos con el id insertado
		for (contador1 = 0; contador1 < table.length; contador1++) {
			trs = w3.getElements(sel);//coge los selectores
			for (contador2 = 0; contador2 < trs.length; contador2++) {//recorre todos los seleccionados del segundo parametro
				existe = 0;
				1
				interior_tr = trs[contador2].getElementsByTagName("span");
				for (contador3 = 0; contador3 < interior_tr.length; contador3++) {
					if (interior_tr[contador3].innerHTML.toUpperCase().trim().indexOf(filter.toUpperCase().trim()) > -1) {
						existe = 1;
					}
				}
				if (existe == 1) { //esconde o no el
					trs[contador2].style.display = "";
				} else {
					trs[contador2].style.display = "none";
				}
			}
		}
	};

	$(".borrable").on('click', function () {
		var id = $(this).data('id');
		$('[name="id"]').val(id);
		$('#borrar').html(id);

	});

	$('.tipos-mostrar').css("display", "none");

	$('#tipos_select').on(
		'change',
		function () {
			$('.tipos-mostrar').css("display", "none");
			$(".tipos-mostrar").removeAttr("selected");
			var tipo = $('#tipos_select').val();
			$('.tipo-' + tipo).css("display", "block");
			$('.tipo-' + tipo).first().attr("selected", "selected");
		}
	);
}
