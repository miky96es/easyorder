<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_admin extends CI_Controller
{
	public $infouser;
	public $headerdata;

	public function __construct()
	{
		parent::__construct();
		if ($_SESSION['cargo'] != 1) {
			redirect(base_url('c_login'));
		}
		$foto = file_exists('files/user_files/images/' . $_SESSION['url_foto_usuario']) && !is_null($_SESSION['url_foto_usuario']) ? base_url('files/user_files/images/' . $_SESSION['url_foto_usuario']) : base_url('assets/restaurant-icons/032-user.png');
		$this->infouser = array(
			'nombre' => $_SESSION['nombre'],
			'apellidos' => $_SESSION['apellidos'],
			'movil' => $_SESSION['movil'],
			'cargo' => 'Administrador',
			'email' => $_SESSION['email'],
			'fecha_nac' => $_SESSION['fecha_nac'],
			'nacionalidad' => $_SESSION['nacionalidad'],
			'sueldo' => $_SESSION['sueldo'],
			'foto' => $foto
		);
		$this->headerdata = array(
			"titulo" => "EasyOrder " . ucfirst(str_replace('c_', '', ($this->router->fetch_class()))) . " - " . str_replace('_', ' ', ucfirst($this->router->fetch_method()))
		);
	}

	public function index()
	{
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/admin');
		$this->load->view('templates/footer');
	}

	public function list_tables()
	{
		//$this->load->library('pagination');
		$this->load->model('m_mesa');

		//$config['base_url'] = base_url('c_admin/list_tables');
		//$config['total_rows'] = 200;
		//$config['per_page'] = 20;
		$data = array("mesas" => $this->m_mesa->get_mesas());
		//$this->pagination->initialize($config);
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/table/list', $data);
		$this->load->view('templates/footer');
	}

	public function add_table()
	{
		if (isset($_POST['n_comensales'])) {
			$this->load->model('m_mesa');
			$this->m_mesa->add_mesa(array('num_comensales' => $_POST['n_comensales']));
		}
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/table/add');
		$this->load->view('templates/footer');
	}

	public function edit_table($id)
	{
		$editado = false;
		$existe = false;
		$this->load->model('m_mesa');
		if (isset($_POST['n_comensales'])) {
			$array_mesa = array(
				'id' => $id,
				'num_comensales' => $_POST['n_comensales']);
			$editado = $this->m_mesa->edit_mesa($array_mesa);
		}
		$mesa = $this->m_mesa->get_mesa($id);
		if (is_array($mesa)) {
			$existe = true;
		}
		$array_editar = array(
			'existe' => $existe,
			'mesa' => $mesa,
			'editado' => $editado
		);
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/table/edit', $array_editar);
		$this->load->view('templates/footer');
	}

	public function delete_table()
	{
		if (isset($_POST['id'])) {
			$this->load->model('m_mesa');
			$infoborrado = array(
				'borrado' => $this->m_mesa->del_mesa($_POST['id']),
				'id' => $_POST['id']);
			$this->session->set_flashdata($infoborrado);
			redirect(base_url('c_admin/list_tables'));
		}
	}

	public function list_users()
	{
		$this->load->model('m_usuario');
		$data = array("usuarios" => $this->m_usuario->get_usuarios());
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/user/list', $data);
		$this->load->view('templates/footer');

	}

	public function add_user()
	{
		$config = array(
			array(
				'field' => 'id',
				'label' => 'dni',
				'rules' => 'is_unique[USUARIO.id]|callback_dni_check|required|min_length[9]|max_length[9]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'is_unique' => 'Este %s ya existe.',
					'dni_check' => 'Este dni/nie no cumple el formato.',
					'min_length' => 'Ha de tener mínimo 9 caracteres.',
					'max_length' => 'Ha de tener máximo 9 caracteres.'
				)
			),
			array(
				'field' => 'nombre',
				'label' => 'nombre',
				'rules' => 'required|min_length[2]|max_length[50]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'min_length' => 'El %s ha de tener mínimo 2 caracteres.',
					'max_length' => 'El %s ha de tener máximo 50 caracteres.'
				)
			),
			array(
				'field' => 'apellidos',
				'label' => 'apellidos',
				'rules' => 'required|min_length[2]|max_length[100]',
				'errors' => array(
					'required' => 'Debes introducir los %s.',
					'min_length' => 'Los %s ha tener mínimo 2 caracteres.',
					'max_length' => 'Los %s ha tener máximo 100 caracteres.'
				)
			),
			array(
				'field' => 'movil',
				'label' => 'móvil',
				'rules' => 'required|is_unique[USUARIO.movil]|min_length[2]|max_length[14]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'is_unique' => 'Este %s ya esta en uso.',
					'min_length' => 'El %s ha de tener mínimo 2 caracteres.',
					'max_length' => 'El %s ha de tener máximo 14 caracteres.'
				)
			), array(
				'field' => 'cargo',
				'label' => 'cargo',
				'rules' => 'required',
				'errors' => array(
					'required' => 'Debes introducir el %s.'
				)
			), array(
				'field' => 'pass',
				'label' => 'contraseña',
				'rules' => 'required|min_length[2]|max_length[50]',
				'errors' => array(
					'required' => 'Debes introducir la %s.',
					'min_length' => 'La %s ha de tener mínimo 2 caracteres.',
					'max_length' => 'La %s ha de tener máximo 50 caracteres.'

				)
			), array(
				'field' => 'email',
				'label' => 'email',
				'rules' => 'required|is_unique[USUARIO.email]|max_length[150]|valid_email',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'is_unique' => 'Este %s ya existe.',
					'max_length' => 'La %s ha de tener máximo 50 caracteres.',
					'valid_email' => 'El %s no cumple el formato.'
				)
			), array(
				'field' => 'fecha_nac',
				'label' => 'fecha de nacimiento',
				'rules' => 'required',
				'errors' => array(
					'required' => 'Debes introducir la %s.'
				)
			), array(
				'field' => 'nacionalidad',
				'label' => 'nacio',
				'rules' => 'required|max_length[50]|min_length[2]',
				'errors' => array(
					'required' => 'Debes introducir la %s.',
					'max_length' => 'La %s ha de tener máximo 50 caracteres.',
					'min_length' => 'La %s ha de tener mínimo 2 caracteres.',

				)
			), array(
				'field' => 'sueldo',
				'label' => 'sueldo',
				'rules' => 'required|greater_than[299.99]|less_than[10000]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'greater_than' => 'El %s no puede ser menor a 300.',
					'less_than' => 'El $s no puede ser mayor a 9999.99'
				)
			)
		);
		$this->form_validation->set_rules($config);

		if ($this->form_validation->run()) {
			$config_photo['upload_path'] = './files/user_files/images/';
			$config_photo['allowed_types'] = 'jpg|png';
			$config_photo['file_name'] = mb_strtoupper($_POST['id']);
			$config_photo['file_ext_tolower'] = true;
			$config_photo['overwrite'] = true;
			$this->load->library('upload', $config_photo);
			$imagen_subida = $this->upload->do_upload('url_foto_usuario');
			$creado = false;
			$this->load->model('m_usuario');
			$_POST['id'] = mb_strtoupper($_POST['id']);
			$_POST['nacionalidad'] = mb_strtoupper($_POST['nacionalidad']);
			if ($imagen_subida) {
				$_POST['url_foto_usuario'] = empty($this->upload->data('file_name')) ? null : $this->upload->data('file_name');
				$creado = $this->m_usuario->add_user($_POST);
				if ($creado) {
					$info_creado = array(
						'creado' => $creado,
						'dni_creado' => $_POST['id']
					);
					$this->session->set_flashdata($info_creado);
				}
			} else {
				$_POST['url_foto_usuario'] = null;
				$creado = $this->m_usuario->add_user($_POST);

				$info_creado = array(
					'creado' => $creado,
					'dni_creado' => $_POST['id']
				);
				$this->session->set_flashdata($info_creado);
			}
		}
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/user/add');
		$this->load->view('templates/footer');
	}

	public function edit_user($id)
	{
		$config = array(
			array(
				'field' => 'nombre',
				'label' => 'nombre',
				'rules' => 'required|min_length[2]|max_length[50]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'min_length' => 'El %s ha de tener mínimo 2 caracteres.',
					'max_length' => 'El %s ha de tener máximo 50 caracteres.'
				)
			),
			array(
				'field' => 'apellidos',
				'label' => 'apellidos',
				'rules' => 'required|min_length[2]|max_length[100]',
				'errors' => array(
					'required' => 'Debes introducir los %s.',
					'min_length' => 'Los %s ha tener mínimo 2 caracteres.',
					'max_length' => 'Los %s ha tener máximo 100 caracteres.'
				)
			),
			array(
				'field' => 'movil',
				'label' => 'móvil',
				'rules' => 'required|min_length[2]|max_length[14]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'is_unique' => 'Este %s ya esta en uso.',
					'min_length' => 'El %s ha de tener mínimo 2 caracteres.',
					'max_length' => 'El %s ha de tener máximo 14 caracteres.'
				)
			), array(
				'field' => 'cargo',
				'label' => 'cargo',
				'rules' => 'required',
				'errors' => array(
					'required' => 'Debes introducir el %s.'
				)
			), array(
				'field' => 'pass',
				'label' => 'contraseña',
				'rules' => 'required|min_length[2]|max_length[50]',
				'errors' => array(
					'required' => 'Debes introducir la %s.',
					'min_length' => 'La %s ha de tener mínimo 2 caracteres.',
					'max_length' => 'La %s ha de tener máximo 50 caracteres.'

				)
			), array(
				'field' => 'email',
				'label' => 'email',
				'rules' => 'required|max_length[150]|valid_email',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'is_unique' => 'Este %s ya existe.',
					'max_length' => 'La %s ha de tener máximo 50 caracteres.',
					'valid_email' => 'El %s no cumple el formato.'
				)
			), array(
				'field' => 'fecha_nac',
				'label' => 'fecha de nacimiento',
				'rules' => 'required',
				'errors' => array(
					'required' => 'Debes introducir la %s.'
				)
			), array(
				'field' => 'nacionalidad',
				'label' => 'nacio',
				'rules' => 'required|max_length[50]|min_length[2]',
				'errors' => array(
					'required' => 'Debes introducir la %s.',
					'max_length' => 'La %s ha de tener máximo 50 caracteres.',
					'min_length' => 'La %s ha de tener mínimo 2 caracteres.',

				)
			), array(
				'field' => 'sueldo',
				'label' => 'sueldo',
				'rules' => 'required|greater_than[299.99]|less_than[10000]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'greater_than' => 'El %s no puede ser menor a 300.',
					'less_than' => 'El $s no puede ser mayor a 9999.99'
				)
			)
		);
		$this->form_validation->set_rules($config);
		$editado = false;
		$existe = false;
		$enviado = false;
		$this->load->model('m_usuario');
		$usuario = $this->m_usuario->get_user($id);
		if (isset($_POST)) {
			if ($this->form_validation->run()) {
				$config_photo['upload_path'] = './files/user_files/images/';
				$config_photo['allowed_types'] = 'jpg|png';
				$config_photo['file_name'] = mb_strtoupper($id);
				$config_photo['file_ext_tolower'] = true;
				$config_photo['overwrite'] = true;
				$this->load->library('upload', $config_photo);
				$imagen_subida = $this->upload->do_upload('url_foto_usuario');
				$this->load->model('m_usuario');
				$_POST['id'] = mb_strtoupper($id);
				$_POST['nacionalidad'] = mb_strtoupper($_POST['nacionalidad']);
				if ($imagen_subida) {
					$_POST['url_foto_usuario'] = empty($this->upload->data('file_name')) ? $usuario['url_foto_usuario'] : $this->upload->data('file_name');
				} else {
					$_POST['url_foto_usuario'] = $usuario['url_foto_usuario'];
				}
				$enviado = true;
				$editado = $this->m_usuario->edit_user($_POST);
				$usuario = $this->m_usuario->get_user($id);
			}
		}
		if (is_array($usuario)) {
			$existe = true;
		}
		$array_editar = array(
			'existe' => $existe,
			'usuario' => $usuario,
			'editado' => $editado,
			'enviado' => $enviado
		);
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/user/edit', $array_editar);
		$this->load->view('templates/footer');
	}

	public function delete_user()
	{
		if (isset($_POST['id'])) {
			$this->load->model('m_usuario');
			$infoborrado = array(
				'borrado' => $this->m_usuario->del_usuario($_POST['id']),
				'id' => $_POST['id']);
			$this->session->set_flashdata($infoborrado);
			redirect(base_url('c_admin/list_users'));
		}
	}

	public function list_products()
	{
		$this->load->model('m_producto');
		$data = array("productos" => $this->m_producto->get_productos());
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/product/list', $data);
		$this->load->view('templates/footer');
	}

	public function add_product()
	{
		$config = array(
			array(
				'field' => 'nombre',
				'label' => 'nombre',
				'rules' => 'required|min_length[2]|max_length[100]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'min_length' => 'El %s ha de tener mínimo 2 caracteres.',
					'max_length' => 'El %s ha de tener máximo 100 caracteres.'
				)
			),
			array(
				'field' => 'id_subtipo_producto',
				'label' => 'subtipo',
				'rules' => 'required',
				'errors' => array(
					'required' => 'Debes introducir el %s.'
				)
			),
			array(
				'field' => 'descripcion',
				'label' => 'descripción',
				'rules' => 'required|min_length[2]|max_length[200]',
				'errors' => array(
					'required' => 'Debes introducir la %s.',
					'min_length' => 'Los %s ha tener mínimo 2 caracteres.',
					'max_length' => 'Los %s ha tener máximo 200 caracteres.'
				)
			),
			array(
				'field' => 'precio',
				'label' => 'precio',
				'rules' => 'required|greater_than[0.00]|less_than[999.99]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'greater_than' => 'El %s no puede ser menor a 300.',
					'less_than' => 'El $s no puede ser mayor a 9999.99'
				)
			));

		$this->load->model('m_producto');
		$this->load->model('m_tipo_producto');
		$this->load->model('m_subtipo_producto');
		$tipos_to_form = array();
		foreach ($this->m_tipo_producto->get_tipos() as $entrada) {
			$tipos_to_form[$entrada['id']] = $entrada['nombre'];
		};

		$subtipos_to_form = array();
		foreach ($this->m_subtipo_producto->get_subtipos() as $entrada) {
			$subtipos_to_form[$entrada['id']] = $entrada['nombre'];
		}
		$info = array(
			'tipos_to_form' => $tipos_to_form,
			'subtipos_to_form' => $subtipos_to_form,
			'tipos' => $this->m_tipo_producto->get_tipos(),
			'subtipos' => $this->m_subtipo_producto->get_subtipos()
		);

		$this->form_validation->set_rules($config);
		if ($this->form_validation->run()) {
			unset($_POST['tipo']);
			$config_photo['upload_path'] = './assets/images/productos';
			$config_photo['allowed_types'] = 'jpg|png';
			$config_photo['file_name'] = mb_strtoupper($_POST['nombre'].date ('Y-m-d_H-i-s'));
			$config_photo['file_ext_tolower'] = true;
			$config_photo['overwrite'] = true;
			$this->load->library('upload', $config_photo);
			$imagen_subida = $this->upload->do_upload('url_foto_producto');
			unset($_POST['tipo']);
			if ($imagen_subida) {
				$_POST['url_foto_producto'] = empty($this->upload->data('file_name')) ? null : $this->upload->data('file_name');
				$creado = $this->m_producto->add_producto($_POST);
				if ($creado) {
					$info_creado = array(
						'creado' => $creado,
						'nombre_prod' => $_POST['nombre']
					);
					$this->session->set_flashdata($info_creado);
				}
			} else {
				$_POST['url_foto_producto'] = null;
				$creado = $this->m_producto->add_producto($_POST);

				$info_creado = array(
					'creado' => $creado,
					'nombre_prod' => $_POST['nombre']
				);
				$this->session->set_flashdata($info_creado);
			}
		}

		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/product/add', $info);
		$this->load->view('templates/footer');
	}

	public function edit_product($id)
	{
		$config = array(
			array(
				'field' => 'nombre',
				'label' => 'nombre',
				'rules' => 'required|min_length[2]|max_length[100]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'min_length' => 'El %s ha de tener mínimo 2 caracteres.',
					'max_length' => 'El %s ha de tener máximo 100 caracteres.'
				)
			),
			array(
				'field' => 'id_subtipo_producto',
				'label' => 'subtipo',
				'rules' => 'required',
				'errors' => array(
					'required' => 'Debes introducir el %s.'
				)
			),
			array(
				'field' => 'descripcion',
				'label' => 'descripción',
				'rules' => 'required|min_length[2]|max_length[200]',
				'errors' => array(
					'required' => 'Debes introducir la %s.',
					'min_length' => 'Los %s ha tener mínimo 2 caracteres.',
					'max_length' => 'Los %s ha tener máximo 200 caracteres.'
				)
			),
			array(
				'field' => 'precio',
				'label' => 'precio',
				'rules' => 'required|greater_than[0.00]|less_than[999.99]',
				'errors' => array(
					'required' => 'Debes introducir el %s.',
					'greater_than' => 'El %s no puede ser menor a 300.',
					'less_than' => 'El $s no puede ser mayor a 9999.99'
				)
			));
		$this->form_validation->set_rules($config);
		$editado = false;
		$existe = false;
		$enviado = false;
		$this->load->model('m_producto');
		$this->load->model('m_tipo_producto');
		$this->load->model('m_subtipo_producto');
		$tipos_to_form = array();
		foreach ($this->m_tipo_producto->get_tipos() as $entrada) {
			$tipos_to_form[$entrada['id']] = $entrada['nombre'];
		};

		$subtipos_to_form = array();
		foreach ($this->m_subtipo_producto->get_subtipos() as $entrada) {
			$subtipos_to_form[$entrada['id']] = $entrada['nombre'];
		}
		$producto = $this->m_producto->get_producto($id);
		if (isset($_POST)) {
			if ($this->form_validation->run()) {
				$config_photo['upload_path'] = './assets/images/productos';
				$config_photo['allowed_types'] = 'jpg|png';
				$config_photo['file_name'] = mb_strtoupper($_POST['nombre'].date ('Y-m-d_H-i-s'));
				$config_photo['file_ext_tolower'] = true;
				$config_photo['overwrite'] = true;
				$this->load->library('upload', $config_photo);
				$imagen_subida = $this->upload->do_upload('url_foto_producto');
				unset($_POST['tipo']);
				if ($imagen_subida) {
					$_POST['url_foto_producto'] = empty($this->upload->data('file_name')) ? $producto['url_foto_producto'] : $this->upload->data('file_name');
				} else {
					$_POST['url_foto_producto'] = $producto['url_foto_producto'];
				}
				$_POST['id']=$producto['id'];
				$enviado = true;
				$editado = $this->m_producto->edit_producto($_POST);
				$producto = $this->m_producto->get_producto($id);

			}
		}
		if (is_array($producto)) {
			$existe = true;
		}
		$info = array(
			'tipos_to_form' => $tipos_to_form,
			'subtipos_to_form' => $subtipos_to_form,
			'tipos' => $this->m_tipo_producto->get_tipos(),
			'subtipos' => $this->m_subtipo_producto->get_subtipos(),
			'producto' => $producto,
			'existe' => $existe,
			'editado' => $editado,
			'enviado' => $enviado
		);
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/product/edit',$info);
		$this->load->view('templates/footer');
	}

	public function delete_product()
	{
		if (isset($_POST['id'])) {
			$this->load->model('m_producto');
			$infoborrado = array(
				'borrado' => $this->m_producto->del_producto($_POST['id']),
				'id' => $_POST['id']);
			$this->session->set_flashdata($infoborrado);
			redirect(base_url('c_admin/list_products'));
		}
	}

	public function list_menus()
	{
		$this->load->model('m_menu');
		$data = array("menus" => $this->m_menu->get_menus());
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/menu/list', $data);
		$this->load->view('templates/footer');

	}

	public function add_menu()
	{

	}

	public function edit_menu()
	{

	}

	public function delete_menu()
	{
		if (isset($_POST['id'])) {
			$this->load->model('m_menu');
			$infoborrado = array(
				'borrado' => $this->m_menu->del_menu($_POST['id']),
				'id' => $_POST['id']);
			$this->session->set_flashdata($infoborrado);
			redirect(base_url('c_admin/list_menus'));
		}
	}

	public function list_reservations()
	{
		$this->load->model('m_reserva');
		$data = array("usuarios" => $this->m_reserva->get_reservas());
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/reservation/list', $data);
		$this->load->view('templates/footer');

	}

	public function list_orders()
	{
		$this->load->model('m_pedido');
		$data = array("pedidos" => $this->m_pedido->get_pedidos());
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('admin/order/list', $data);
		$this->load->view('templates/footer');

	}

	public function statistics()
	{
		$this->load->view('templates/admin_header', $this->headerdata);
		$this->load->view('admin/admin_navbar', $this->infouser);
		$this->load->view('templates/footer');

	}

	public function dni_check($dni)
	{
		$dni = trim(strtoupper($dni));
		if (strlen($dni) < 9) {
			return false;
		} else {
			$letra = substr($dni, -1, 1);
			$numero = substr($dni, 0, 8);
			$numero = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $numero);
			$modulo = $numero % 23;
			$letras_validas = "TRWAGMYFPDXBNJZSQVHLCKE";
			$letra_correcta = substr($letras_validas, $modulo, 1);
			if ($letra_correcta != $letra) {
				return false;
			} else {
				return true;
			}
		}
	}
}
