<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_usuario');
	}

	public function index()
	{
		switch ($this->session->userdata('cargo')) {
			case 1:
				redirect(base_url('c_admin'));
				break;
			case 2:
				redirect(base_url('c_cocinero'));
				break;
			case 3:
				redirect(base_url('c_camarero'));
				break;
			default:
				$headerdata = array(
					"titulo" => "EasyOrder " . ucfirst(str_replace('c_','',($this->router->fetch_class()))) . " - " . str_replace('_', ' ', ucfirst($this->router->fetch_method()))
				);
				$this->load->view('templates/login_header', $headerdata);
				$this->load->view('login/login');
				$this->load->view('templates/footer');
				break;
		}
	}

	public function enter()
	{
		$config = array(
			array(
				'field' => 'dni',
				'label' => 'dni',
				'rules' => 'required',
				'errors' => array(
					'required' => 'Debes introducir tu %s.'
				)
			),
			array(
				'field' => 'password',
				'label' => 'contraseña',
				'rules' => 'required',
				'errors' => array(
					'required' => 'Debes introducir tu %s.'
				)
			)
		);
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE) {
			$this->index();
		} else {
			$dni = $this->input->post('dni');
			$password = $this->input->post('password');
			$check_user = $this->M_usuario->is_user($dni, $password);
			if ($check_user == TRUE) {
				$data = $this->M_usuario->get_user($dni);
				$this->session->set_userdata($data);
			}
			redirect(base_url('c_login'));
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->index();
	}

}
