<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_camarero extends CI_Controller{
	public $infouser;
	public $headerdata;
	public function __construct(){
		parent::__construct();
		$this->load->model('m_mesa');
		$this->load->model('m_producto');
		$this->load->model('m_pedido');
		$this->load->model('m_menu');
		if ($_SESSION['cargo'] != 3) {
			redirect(base_url('c_login'));
		}
		$this->load->model('m_mesa');
		$this->infouser = array(
			'nombre' => $_SESSION['nombre'],
			'apellidos' => $_SESSION['apellidos'],
			'movil' => $_SESSION['movil'],
			'cargo' => 'Administrador',
			'email' => $_SESSION['email'],
			'fecha_nac' => $_SESSION['fecha_nac'],
			'nacionalidad' => $_SESSION['nacionalidad'],
			'sueldo' => $_SESSION['sueldo'],
			'foto' => $_SESSION['url_foto_usuario']
		);

		$this->headerdata = array(
			"titulo" => "EasyOrder " . ucfirst(str_replace('c_','',($this->router->fetch_class()))) . " - " . str_replace('_', ' ', ucfirst($this->router->fetch_method()))
		);
	}

	public function index(){
		$infoMesas = array(
			'infoMesas'=> $this->m_mesa->get_mesas()
		);
		$this->load->view('templates/camarero_header', $this->headerdata);
		$this->load->view('camarero/camarero_navbar',$this->infouser);
		$this->load->view('camarero/body',$infoMesas);
		//
		//$this->load->view('templates/footer');
	}

	/*================= PEDIDOS ===============*/

	/*public function pedidos(){
		$infoMesas = array(
			'infoMesas'=> $this->m_mesa->get_mesas()
		);
		$this->load->view('templates/camarero_header',$this->headerdata);
		$this->load->view('camarero/camarero_navbar',$this->infouser);
		$this->load->view('camarero/pedidos',$infoMesas);
		//$this->load->view('camarero/opcion_mesa',array("id_mesa"=> $id_mesa));
	}

	public function listado_pedidos(){
		$infoMesas = array(
			'infoMesas'=> $this->m_mesa->get_mesas()
		);
		$this->load->view('templates/camarero_header',$this->headerdata);
		$this->load->view('camarero/camarero_navbar',$this->infouser);
		$this->load->view('camarero/listado_pedidos',$infoMesas);
		//$this->load->view('camarero/opcion_mesa',array("id_mesa"=> $id_mesa));
	}*/

	//Función que da de alta un pedido asociado a una mesa
	public function ajax_alta_pedido(){
		$res = false;
		$mesa= $this->input->post('mesa');
		$productos= $this->input->post('productos');
		$array_productos = explode(",", $productos);
		$id_pedido = $this->m_pedido->alta_pedido($mesa);
		if($id_pedido > 0 && $id_pedido != null){
			for ($i=0; $i< count($array_productos)-1;$i++){
				$insert = $this->m_pedido->insert_productos_pedido($id_pedido,$array_productos[$i]);
				if($insert == false){
					break;
				}
				else{
					$res = true;
				}
			}
		}
		echo $res;
	}

	//Función que devuelve el pedido actual de una mesa
	public function ajax_pedido_realizado($id_mesa){
		$infoPedido = array(
			'infoPedido'=> $this->m_pedido->get_pedido_realizado($id_mesa)
		);
		$this->load->view('camarero/listado_ordenes',$infoPedido);
		//$this->load->view('camarero/opcion_mesa',array("id_mesa"=> $id_mesa));
	}

	public function ajax_tiene_pedido($id_mesa){
		$res = 0;
		$infoPedido = $this->m_pedido->tiene_pedido($id_mesa);
		if($infoPedido > 0){
			$res = 1;
		}
		echo $res;
		//$this->load->view('camarero/opcion_mesa',array("id_mesa"=> $id_mesa));
	}



	/*=============== MESAS ==================================*/

	/*Consulta al modelo Mesa el estado de una mesa 1(desocupada) o 2 (ocupada)*/
	public function get_estado_mesa($id){
		$query = $this->m_mesa->get_estado($id);
		echo json_encode($query);
	}

	/*Solicita al modelo Mesa el cambio a 2 (ocupada)*/
	public function ajax_set_estado_ocupada($id_mesa){
		$estado = array('estado' => 2);
		$query = $this->m_mesa->set_estado($id_mesa,$estado);
		echo json_encode($query);
	}

	/*Solicita al modelo Mesa el cambio a 1 (desocupada)*/
	public function ajax_set_estado_desocupada($id_mesa){
		$estado = array('estado' => 1);
		$query = $this->m_mesa->set_estado($id_mesa,$estado);
		echo json_encode($query);
	}

	/*=============== PRODUCTOS =====================*/

	public function ajax_get_producto($id){
		$infoProductos = array(
			'infoProductos'=> $this->m_producto->ajax_get_producto($id)
		);
		$this->load->view('camarero/info_producto',$infoProductos);
	}

	/*Pide la info de un producto y muestra una vista en forma de tabla*/
	public function ajax_get_producto_tabla($id){
		$infoProductos = array(
			'infoProductos'=> $this->m_producto->ajax_get_producto($id)
		);
		$this->load->view('camarero/listado_productos_pedido',$infoProductos);
	}

	/*Pide al modelo Producto todos los productos que pertenezcan a un subtipo de producto ej Bebidas -> Refrescos*/
	public function ajax_get_subtipo_producto($id){
		$infoProductos = array(
			'infoProductos'=> $this->m_producto->ajax_get_subtipo_producto($id)
		);
		$this->load->view('camarero/detalles_subtipo_producto',$infoProductos);
	}

	/*Pide al modelo todos los productos que pertenezcan a un tipo de producto ejm entrantes*/
	public function ajax_get_tipo_producto($tipo){
		$infoProductos = array(
			'infoProductos'=> $this->m_producto->get_tipo_producto($tipo)
		);
		$this->load->view('camarero/detalles_tipo_producto',$infoProductos);
	}

	/*=============== MENUS ===================*/

	/*Pidel al modelo entrantes que devuelva un listado de los menús existentes*/

	public function ajax_get_menus(){
		$infoMenus = array(
			'infoMenus'=> $this->m_menu->get_menus()
		);
		$this->load->view('camarero/menus',$infoMenus);
	}

	public function ajax_get_menu($id){
		$infoMenu = array(
			'infoMenu'=> $this->m_menu->ajax_get_menu($id)
		);
		$this->load->view('camarero/info_menu',$infoMenu);
	}

	public function ajax_get_productos_menu($id){
		$infoMenu = array(
			'infoProductos'=> $this->m_menu->ajax_get_productos_menu($id)
		);
		$this->load->view('camarero/info_productos_menu',$infoMenu);
	}

	/*============== FACTURA ==================*/
	public function ajax_generar_factura($id_mesa){
		$infoPedido = array(
			'infoPedido'=> $this->m_pedido->get_pedido_realizado($id_mesa)
		);
		$this->load->view('camarero/factura',$infoPedido);
	}

	public function ajax_marcar_pagado($id_mesa){
		$estado_pago= array('estado_pago' => 2);
		$query = $this->m_pedido->set_estado($id_mesa, $estado_pago);
		echo json_encode($query);
	}

	public function ajax_cancelar_orden($id_orden){
		$estado_producto= array('estado_producto' => 5);
		$query = $this->m_pedido->set_estado_orden($id_orden, $estado_producto);
		echo json_encode($query);
	}

	public function ajax_entregar_orden($id_orden){
		$estado_producto= array('estado_producto' => 4);
		$query = $this->m_pedido->set_estado_orden($id_orden, $estado_producto);
		echo json_encode($query);
	}

}
