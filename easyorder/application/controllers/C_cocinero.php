<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_cocinero extends CI_Controller
{
	public $infouser;
	public $headerdata;
	public function __construct(){
		parent::__construct();
		$this->load->model('m_pedido');
		$this->load->model('m_linea_pedido');
		if ($_SESSION['cargo'] != 2) {
			redirect(base_url('c_login'));
		}
		//$this->load->model('m_mesa');
		$this->infouser = array(
			'nombre' => $_SESSION['nombre'],
			'apellidos' => $_SESSION['apellidos'],
			'movil' => $_SESSION['movil'],
			'cargo' => 'Administrador',
			'email' => $_SESSION['email'],
			'fecha_nac' => $_SESSION['fecha_nac'],
			'nacionalidad' => $_SESSION['nacionalidad'],
			'sueldo' => $_SESSION['sueldo'],
			'foto' => $_SESSION['url_foto_usuario']
		);

		$this->headerdata = array(
			"titulo" => "EasyOrder " . ucfirst(str_replace('c_','',($this->router->fetch_class()))) . " - " . str_replace('_', ' ', ucfirst($this->router->fetch_method()))
		);
	}

	public function index(){
		/*$infoMesas = array(
			'infoMesas'=> $this->m_mesa->get_mesas()
		);*/
		$this->load->view('templates/cocinero_header', $this->headerdata);
		$this->load->view('cocinero/cocinero_navbar',$this->infouser);

		$pedidos = array(
			'cola' => $this->m_pedido->get_linea_pedido_cocinero_cola(),
			'listos' => $this->m_pedido->get_linea_pedido_cocinero_listos(),
		);

		$this->load->view('cocinero/home_cocinero', $pedidos);
		//
		//$this->load->view('templates/footer');
	}

	public function ajax_get_listado_cola(){
			$pedidos = $this->m_pedido->get_linea_pedido_cocinero_cola();
			echo json_encode($pedidos);
	}

	public function ajax_get_listado_listos(){
			$pedidos = $this->m_pedido->get_linea_pedido_cocinero_listos();
			echo json_encode($pedidos);
	}

	public function set_estado($id){
		$linea = $this->m_linea_pedido->get_linea_pedido_by_id($id);
		$to = $this->input->post('to');
		if($linea && $to){
			$query = $this->m_linea_pedido->update_status($id, $to);
			echo json_encode(array('estado' => 1));
		}else{
			echo json_encode(array('estado' => 0));
		}
	}
}
