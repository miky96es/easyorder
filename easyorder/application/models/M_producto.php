<?php

class M_producto extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_tipo_producto($tipo)
	{
		$this->db->select('*');
		$this->db->where('id_tipo_producto', $tipo);
		$query = $this->db->get('SUBTIPO_PRODUCTO');
		return $query->result_array();
	}

	//Retorna toda la informacion del producto en un array
	public function get_producto($id)
	{
		$this->db->select('id, nombre, descripcion, precio, id_subtipo_producto, url_foto_producto');
		$this->db->where('id', $id);
		$query = $this->db->get('PRODUCTO');
		return $query->row_array();
	}

	//Devuelve la info sobre los productos
	public function get_productos()
	{
		$this->db->select('p.id, p.nombre, p.descripcion, p.precio, t.nombre tipo, s.nombre subtipo, p.url_foto_producto');
		$this->db->from('PRODUCTO p, SUBTIPO_PRODUCTO s, TIPO_PRODUCTO t');
		$this->db->where('p.id_subtipo_producto=s.id and s.id_tipo_producto=t.id');
		$query = $this->db->get();
		return $query->result_array();
	}


	/*Devuelve todos los menus que haya*/
	public function get_menus(){
		$this->db->select('*');
		$query = $this->db->get('MENU');
		return $query->result_array();
	}


	/*Devuelve todos los productos que pertenezcan a un subtipo ej refrescos*/
	public function ajax_get_subtipo_producto($id){
		$this->db->select('*');
		$this->db->where('id_subtipo_producto', $id);
		$query = $this->db->get('PRODUCTO');
		return $query->result_array();
	}

	/*Devuelve un producto en concreto */
	public function ajax_get_producto($id){
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('PRODUCTO');
		return $query->result_array();
	}


	/*Devuelve un menu en concreto */
	public function ajax_get_menu($id){
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('MENU');
		return $query->result_array();
	}

	/*Devuelve los productos que compones un menú */
	public function ajax_get_productos_menu($id){
		$sql = "select s.id_tipo_producto,p.id,p.nombre,p.descripcion,p.precio,p.url_foto_producto 
				from SUBTIPO_PRODUCTO s, PRODUCTO p where s.id in(
					select p.id_subtipo_producto from PRODUCTO p WHERE p.id in (
						select id_producto 
						from MENU_COMPONE_PRODUCTO 
						where id_menu =" . $id . ")
				)and s.id = p.id_subtipo_producto";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	//borra un producto a traves del id del producto
	public function del_producto($id){
		$this->db->delete("PRODUCTO", "id='$id'");
		return $this->db->affected_rows();

	}

	//anyade un producto a traves de un parametro que es un array con la info del producto
	public function add_producto($product_array){
		return $this->db->insert("PRODUCTO", $product_array);
	}

	//edita un producto a traves de un array de producto que contiene la info a modificar.
	public function edit_producto($product_array){
		$product_id = $product_array["id"];
		unset($product_array["id"]);
		$this->db->set($product_array);
		$this->db->where("id", $product_id);
		return $this->db->update("PRODUCTO", $product_array);
	}
}

