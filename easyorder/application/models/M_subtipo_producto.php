<?php

class M_subtipo_producto extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_subtipos(){
		$this->db->select('id,nombre,id_tipo_producto,url_foto_subtipo');
		$this->db->from('SUBTIPO_PRODUCTO');
		$query = $this->db->get();
		return $query->result_array();
	}
}
