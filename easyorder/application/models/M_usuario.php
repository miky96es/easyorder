<?php

class M_usuario extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	//Comprueba si hay una fila con el email y la contrasenya coincide
	public function is_user($dni, $password)
	{
		$query = $this->db->query("SELECT * FROM USUARIO WHERE id='$dni' AND BINARY pass='$password'");
		return $query->num_rows() > 0 ? true : false;
	}

	//Retorna toda la informacion del usuario en un array, excepto la contra por seguridad
	public function get_user($user_id)
	{
		$this->db->select('id, movil, nombre, apellidos, pass,cargo, email, fecha_nac, nacionalidad, sueldo, url_foto_usuario');
		$this->db->where('id', $user_id);
		$query = $this->db->get('USUARIO');
		return $query->row_array();
	}

	//Devuelve la info sobre los usuarios (excepto el pwd)
	public function get_usuarios()
	{
		$this->db->select('id, movil, nombre, apellidos, cargo, email, fecha_nac, nacionalidad, sueldo, url_foto_usuario');
		$query = $this->db->get('USUARIO');
		return $query->result_array();
	}

	//anyade un usuario a traves de un parametro que es un array con la info del user
	public function add_user($user_array){
		return $this->db->insert("USUARIO", $user_array);
	}

	//borra un usuario a traves del id del usuario
	public function del_usuario($id){
		$this->db->delete("USUARIO", "id='$id'");
		return $this->db->affected_rows();

	}

	//edita un usuario a traves de un array de usuario que contiene la info a modificar.
	public function edit_user($user_array)
	{
		$user_id = $user_array["id"];
		unset($user_array["id"]);
		$this->db->set($user_array);
		$this->db->where("id", $user_id);
		return $this->db->update("USUARIO", $user_array);
	}
}
