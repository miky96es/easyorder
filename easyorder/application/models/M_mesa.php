<?php
class M_mesa extends CI_Model{
	public function __construct(){
		parent::__construct();
	}

	//Retorna toda la informacion de una mesa en un array
	public function get_mesa($id){
		$this->db->select('id, estado, num_comensales');
		$this->db->where('id', $id);
		$query = $this->db->get('MESA');
		return $query->row_array();
	}

	//Devuelve la info sobre las mesas (número de mesa, estado y número de comensales)
	public function get_mesas(){
		$this->db->select('*');
		$query = $this->db->get('MESA');
		return $query->result_array();
	}

	//anyade una mesa a traves de un parametro que es un array con la info de la mesa
	public function add_mesa($mesa_array){
		$this->db->insert("MESA", $mesa_array);
	}

	//borra una mesa a traves del id de la mesa
	public function del_mesa($id){
		$this->db->delete("MESA", "id=$id");
		return $this->db->affected_rows();

	}

	//edita una mesa a traves de un array de mesa que contiene la info a modificar.
	public function edit_mesa($mesa_array){
		$mesa_id = $mesa_array["id"];
		unset($mesa_array["id"]);
		$this->db->set($mesa_array);
		$this->db->where("id", $mesa_id);
		$this->db->update("MESA", $mesa_array);
		return $this->db->affected_rows();
	}

	//Desvuelve el estado de la mesa 2 ocupada o 1 desocupada
	public function get_estado($id){
		$this->db->select('estado');
		$this->db->where('id', $id);
		$query = $this->db->get('MESA');
		return $query->row();
	}

	public function set_estado($id,$estado){
		$this->db->where('id', $id);
		$query = $this->db->update('MESA',$estado);
		return $this->db->affected_rows();
	}
}
