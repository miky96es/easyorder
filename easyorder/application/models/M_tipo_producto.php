<?php

class M_tipo_producto extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_tipos()
	{
		$this->db->select('id,nombre');
		$this->db->from('TIPO_PRODUCTO');
		$query = $this->db->get();
		return $query->result_array();
	}
}
