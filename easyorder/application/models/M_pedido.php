<?php

class M_pedido extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	//Devuelve la info sobre los pedidos
	public function get_pedidos(){
		$this->db->select('p.id, p.fecha, p.estado_pago, p.id_mesa, u.nombre nombre_user, u.apellidos apellidos_user ');
		$this->db->from('PEDIDO p, USUARIO u');
		$this->db->where('u.id=p.id_usuario');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_linea_pedido_cocinero_cola()
	{
		$this->db->select('l.id, l.id_pedido, l.id_menu, l.estado_producto, p.estado_pago, p.fecha, prod.nombre, prod.descripcion');
		$this->db->from('PEDIDO p');
		$this->db->join('LINEA_PEDIDO l', 'l.id_pedido = p.id', 'left');
		$this->db->join('PRODUCTO prod', 'prod.id = l.id_producto', 'left');
		$this->db->where('p.estado_pago', 1);
		$this->db->where('l.estado_producto >=', 1);
		$this->db->where('l.estado_producto <=', 2);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_linea_pedido_cocinero_listos()
	{
		$this->db->select('l.id, l.id_pedido, l.id_menu, l.estado_producto, p.estado_pago, p.fecha, prod.nombre, prod.descripcion');
		$this->db->from('PEDIDO p');
		$this->db->join('LINEA_PEDIDO l', 'l.id_pedido = p.id', 'left');
		$this->db->join('PRODUCTO prod', 'prod.id = l.id_producto', 'left');
		$this->db->where('p.estado_pago', 1);
		$this->db->where('l.estado_producto =', 3);
		$query = $this->db->get();
		return $query->result_array();
	}

	/*Crea un nuevo pedido y devuelve el id generado en la bd*/
	public function alta_pedido($mesa){
		$data = array(
			'id_mesa' => $mesa,
			'id_usuario' => $_SESSION['id']
		);
		$this->db->insert('PEDIDO', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	/*===== inserta los productos seleccionados en un pedido =====*/
	public function insert_productos_pedido($pedido,$producto){
		$data = array(
			'id_pedido' => $pedido,
			'id_producto' => $producto
		);
		return $this->db->insert('LINEA_PEDIDO', $data);
	}

	/*====== devuelve los productos de un pedido que ha realizado una mesa======= */
	public function get_pedido_realizado($id_mesa){
		$sql = "select l.id as l_id, l.id_pedido as l_id_pedido, l.id_menu as l_id_menu, l.id_producto as l_id_producto, l.estado_producto as l_estado_producto, 
 		pe.id as pe_id, pe.fecha as pe_fecha, pe.estado_pago as pe_estado_pago, pe.id_mesa as pe_id_mesa, pe.id_usuario as pe_id_usuario, 
 		p.id as p_id, p.nombre as p_nombre, p.descripcion as p_descripcion, p.precio as p_precio, p.id_subtipo_producto as p_id_subtipo_producto,  
 		sp.id_tipo_producto as sp_id_tipo_producto
 		from LINEA_PEDIDO l,PEDIDO pe, SUBTIPO_PRODUCTO sp,PRODUCTO p where 
		p.id=l.id_producto and pe.id=l.id_pedido and p.id_subtipo_producto = sp.id 
		and  pe.id_mesa=".$id_mesa." and pe.estado_pago=1";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	/*===== Devuelve true o false si una mesa tiene un pedido actual =====*/
	public function tiene_pedido($mesa){
		$this->db->select('*');
		$this->db->where('id_mesa', $mesa);
		$this->db->where('estado_pago', 1);
		$query = $this->db->get('PEDIDO');
		return $query->num_rows();
	}

	/*===== Cambia el estado de una pedido =====*/
	public function set_estado($id_mesa,$estado){
		$this->db->where('id_mesa', $id_mesa);
		$query = $this->db->update('PEDIDO',$estado);
		return $this->db->affected_rows();
	}

	/*===== Cambia el estado de una orden =====*/
	public function set_estado_orden($id_orden,$estado){
		$this->db->where('id', $id_orden);
		$query = $this->db->update('LINEA_PEDIDO',$estado);
		return $this->db->affected_rows();
	}
}
