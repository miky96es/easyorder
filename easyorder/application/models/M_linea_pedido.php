<?php

class M_linea_pedido extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_linea_pedido_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('LINEA_PEDIDO');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->row_array();
	}

	public function update_status($id, $to)
	{
		$this->db->set('estado_producto', $to);
		$this->db->where('id', $id);
		$this->db->update('LINEA_PEDIDO');
		return true;
	}

}
