<?php

class M_menu extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	/*Devuelve todos los menus que haya*/
	public function get_menus(){
		$this->db->select('*');
		$query = $this->db->get('MENU');
		return $query->result_array();
	}

	/*Devuelve un menu en concreto */
	public function ajax_get_menu($id){
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('MENU');
		return $query->result_array();
	}

	/*Devuelve los productos que compones un menú */
	public function ajax_get_productos_menu($id){
		$sql = "select s.id_tipo_producto,p.id,p.nombre,p.descripcion,p.precio,p.url_foto_producto 
				from SUBTIPO_PRODUCTO s, PRODUCTO p where s.id in(
					select p.id_subtipo_producto from PRODUCTO p WHERE p.id in (
						select id_producto 
						from MENU_COMPONE_PRODUCTO 
						where id_menu =".$id.")
				)and s.id = p.id_subtipo_producto";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	//borra un menu a traves del id del menu
	public function del_menu($id)
	{
		$this->db->delete("MENU", "id='$id'");
		return $this->db->affected_rows();

	}
}
