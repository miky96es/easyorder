<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Error 404</title>
	<link rel="stylesheet" type="text/css" href="http://localhost/assets/bootstrap/css/bootstrap.min.css">
</head>
<body>
<div class="container">
	<div class="row">
		<div class="error-template">
			<h1>Oops!</h1>
			<h2>404 lugar no encontrado</h2>
			<div class="error-details">
				Lo siento, ha ocurrido un error, la página no ha sido encontrada!
				<br>
			</div>
			<div class="error-actions">
				<a href="http://localhost" class="btn btn-primary">
					<i class="icon-home icon-white"></i> Volver al inicio </a>
			</div>
		</div>
	</div>
</div>
</body>
</html>

