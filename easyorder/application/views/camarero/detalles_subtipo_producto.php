<div class="row" id="subtipos">
	<?php
	foreach ($infoProductos as $subtipo) {
		?>
		<div class="col-xs-6 col-sm-3 text-center subtipo" data-id="<?php echo $subtipo['id']; ?>">
			<a href="#">
				<img class="icon4"
					<?php
					if (file_exists('assets/images/productos/' . $subtipo["url_foto_producto"]) &&
					$subtipo["url_foto_producto"] != null){
						?>
						src="<?php echo base_url("assets/images/productos/" . $subtipo["url_foto_producto"]); ?>"
					<?php }else{ ?>
					 src="<?php echo base_url("assets/images/productos/default.png"); ?>"
					 <?php } ?>alt="Foto subtipo">
			</a>
			<h4><?php echo $subtipo['nombre']; ?></h4>
			<div class="detalles-producto" id="producto-<?php echo $subtipo['id']; ?>">
				<div id="descripcion"><?php echo $subtipo['descripcion']; ?></div>
				<div id="precio"><?php echo $subtipo['precio']; ?>€</div>
				<button class="btn btn-success btn-añadir-pedido" data-id="<?php echo $subtipo['id']; ?>"><span class="glyphicon glyphicon-plus"></span><b>Añadir al pedido</b>
				</button>
			</div>
		</div>
	<?php } ?>
</div>






