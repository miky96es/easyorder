<div class="text-center" id="titulo">
	<h1><img class="icon1" alt="icono pedidos"
			 src="http://localhost/assets/restaurant-icons/038-credit-card-green.png">Factura
</div>
<div id="listado">
	<div class="col-xs-12 col-sm-12 text-center">
		<?php foreach ($infoPedido as $producto) {
			echo "<h4>".$producto["pe_fecha"]."</h4>";
			break;
		} ?>
		<table class="table" id="tabla">
			<tr>
				<th><h4>Nombre</h4></th>
				<th><h4>Estado</h4></th>
				<th><h4>Precio</h4></th>

			</tr>
			<?php foreach ($infoPedido as $producto) {?>
				<tr class="estado-<?php echo $producto["l_estado_producto"]?>">

					<td><?php echo $producto["p_nombre"]?></td>
					<td>
						<?php
							if($producto["l_estado_producto"] == 1){
								echo "No preparado";
							}
							else if ($producto["l_estado_producto"] == 2){
								echo "En preparación";
							}
							else if ($producto["l_estado_producto"] == 3){
								echo "Preparado";
							}else if ($producto["l_estado_producto"] == 4){
								echo "Entregado";
							}else{
								echo "Cancelado";
							}
						?>
					</td>
					<td><?php echo $producto["p_precio"]?></td>

				</tr>
			<?php } ?>
		</table>
		<?php
			$precio = 0;
			foreach ($infoPedido as $producto) {
				if($producto["l_estado_producto"] == 4){
					$precio = $precio + $producto["p_precio"];
				}
			}
			echo "<h4>TOTAL: ".$precio."€</h4>";
		?>
		<button class="btn btn-success btn-pagado" data-id="<?php echo $producto["pe_id_mesa"]?>"><b>Marcar como pagado</b></button>
	</div>
</div>



