<div class="text-center" id="titulo">
	<h1><img class="icon1" alt="icono pedidos"
			 src="http://localhost/assets/restaurant-icons/021-catering.png"> PEDIDO
</div>
<div id="listado">
	<div class="col-xs-12 col-sm-12 text-center">
		<table class="table" id="tabla">
			<tr>
				<th><h4>Fecha</h4></th>
				<th><h4>Nombre</h4></th>
				<!--<th><h4>Descripción</h4></th>-->
				<th><h4>Precio</h4></th>
				<th><h4>Estado</h4></th>
				<th><h4>Acción</h4></th>
			</tr>
			<?php foreach ($infoPedido as $producto) { $id_pedido = $producto["pe_id"]?>
				<tr class="estado-<?php echo $producto["l_estado_producto"]?>">
					<td><?php echo $producto["pe_fecha"]?></td>
					<td><?php echo $producto["p_nombre"]?></td>
					<!--<td><?php /*echo $producto["descripcion"]*/?></td>-->
					<td><?php echo $producto["p_precio"]?></td>
					<td>
						<?php
							if($producto["l_estado_producto"] == 1){
								echo "No preparado";
							}
							else if ($producto["l_estado_producto"] == 2){
								echo "En preparación";
							}
							else if ($producto["l_estado_producto"] == 3){
								echo "Preparado";
							}else if ($producto["l_estado_producto"] == 4){
								echo "Entregado";
							}else{
								echo "Cancelado";
							}
						?>
					</td>
					<td>
						<div class="btn-group">
							<?php if ($producto["sp_id_tipo_producto"] == 4 && $producto["l_estado_producto"] == 1){?>
								<button class="btn btn-success btn-sm btn-entregar-orden" data-id="<?php echo $producto["l_id"]?>"><b>Entregar</b></button>
								<button class="btn btn-danger btn-sm btn-cancelar-orden" data-id="<?php echo $producto["l_id"]?>"><b>Cancelar</b></button>
							<?php } ?>
							<?php if ($producto["sp_id_tipo_producto"] != 4){?>
								<?php if ($producto["l_estado_producto"] == 1){?>
									<button class="btn btn-danger btn-sm btn-cancelar-orden" data-id="<?php echo $producto["l_id"]?>"><b>Cancelar</b></button>
								<?php } ?>
								<?php if ($producto["l_estado_producto"] == 3){?>
									<button class="btn btn-success btn-sm btn-entregar-orden" data-id="<?php echo $producto["l_id"]?>"><b>Entregar</b></button>
								<?php } ?>
							<?php } ?>

						</div>
					</td>
				</tr>
			<?php }?>
		</table>
		<button class="btn btn-success btn-add-orden"><span class="glyphicon glyphicon-plus"></span><b>&nbspAñadir nuevo
				producto</b></button>
		<button class="btn btn-success btn-cobrar" data-id="<?php echo $producto["pe_id"];?>"><span class="glyphicon glyphicon-euro"></span><b>&nbspCobrar
				pedido</b></button>
	</div>
</div>



