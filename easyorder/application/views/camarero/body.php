<div id="background-content">
	<div class="text-center" id="titulo">
		<h1><img class="icon1" alt="icono pedidos"
				 src="http://localhost/assets/restaurant-icons/014-waitress.png"> PEDIDOS</h1>
	</div>
	<div class="container-fluid">
		<div class="row" id="mesas">
			<?php foreach ($infoMesas as $mesa) { ?>
				<div class="col-xs-6 col-sm-3 text-center">
					<h3 id="comensales"><img class="icon1"
											 src="<?php echo base_url("assets/images/diners/diner512.png"); ?>"
											 alt="Comensales">
						<?php echo $mesa['num_comensales']; ?>
					</h3>

					<a href="#" data-id="<?php echo $mesa['id']; ?>" class="mesa">
						<img class="img-responsive img-mesa"
							<?php if ($mesa["estado"] == 1){ ?>
								src="<?php echo base_url("assets/images/tables/empty_table.png"); ?>"
							<?php }else{ ?>
							 src="<?php echo base_url("assets/images/tables/full_table.png"); ?>"
							 <?php } ?>alt="Mesa">
					</a>
					<h3 id="numero-mesa">#<?php echo $mesa['id'] ?></h3>
				</div>

			<?php } ?>
		</div>

		<div class="row" id="nuevo_pedido">
			<div class="text-center" id="titulo-nuevo-pedido">
				<h2>
					<img class="icon2" alt="icono pedidos" src="http://localhost/assets/restaurant-icons/021-catering.png"> NUEVO PEDIDO
					<!--<button class="btn btn-success btn-cambio-mesa-desocupada" style="text-align: right">Cambiar a desocupada</button>-->
				</h2>

			</div>
			<div class="col-xs-12 col-sm-12 text-center" id="tipo-productos">
				<div>
					<div id="img1" class="entrantes">
						<a href="#">
							<img class="icon3" alt="icono entrantes"
								 src="<?php echo base_url("assets/images/camarero/starter.png"); ?>">
							<h4>Entrantes</h4>
						</a>
					</div>
					<div id="img2" class="platos">
						<a href="#">
							<img class="icon3" alt="icono platos"
								 src="<?php echo base_url("assets/images/camarero/salver.png"); ?>">
							<h4>Platos</h4>
						</a>

					</div>
					<div id="img3" class="bebidas">
						<a href="#">
							<img class="icon3" alt="icono bebidas"
								 src="<?php echo base_url("assets/images/camarero/drinks.png"); ?>">
							<h4>Bebidas</h4>
						</a>

					</div>
					<div id="img4" class="postres">
						<a href="#">
							<img class="icon3" alt="icono postres"
								 src="<?php echo base_url("assets/images/camarero/dessert.png"); ?>">
							<h4>Postres</h4>
						</a>

					</div>
					<div id="img5" class="menus">
						<a href="#">
							<img class="icon3" alt="icono entrantes"
								 src="<?php echo base_url("assets/images/camarero/menu.png"); ?>">
							<h4>Menús</h4>
						</a>
					</div>
				</div>

			</div>

			<div class="col-xs-12 col-sm-12 text-center color1_target" id="productos-listado-acordeon">
				<h3 id="tipo-producto"></h3>
				<div id="detalle_tipo_producto"></div>
				<h4>Pedido</h4>
				<div id="detalles-pedido">
					<table class="table table-hover" id="tabla_listado_productos_pedido">
						<tr>
							<th>Nombre</th>
							<th>Descripción</th>
							<th>Precio</th>
							<th>Cantidad</th>
						</tr>
					</table>
					<div id="ids-productos"></div>
					<button class="btn btn-success btn-enviar-pedido"><span class="glyphicon glyphicon-send"></span><b>&nbspEnviar
							pedido</b></button>
				</div>
			</div>
			<div id="dialog-pedido-ok" title="Pedido registrado">
				<p>
					<span class="ui-icon ui-icon-circle-check"></span>
					El pedido ha sido registrado correctamente.
				</p>
			</div>
			<div id="dialog-pedido-error" title="Pedido no registrado">
				<p>
					<span class="ui-icon ui-icon-alert"></span>
					Ha ocurrido un error.
				</p>
			</div>
		</div>
		<div class="row" id="pedido-realizado"></div>
		<div id="dialog-cancelar" title="¿Cancelar orden?">
			<p>
				<span class="ui-icon ui-icon-circle-check"></span>
				¿Estás seguro de cancelar la orden?
			</p>
		</div>
		<div id="dialog-cancelar-ok" title="¿Cancelar orden?">
			<p>
				<span class="ui-icon ui-icon-circle-check"></span>
				La orden se ha cancelado correctamente
			</p>
		</div>
		<div class="row" id="factura">
		</div>
	</div>
</div>
