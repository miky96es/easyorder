<div id="background-content">
	<div class="text-center" id="titulo">
		<h1><img class="icon1" alt="icono pedidos"
				 src="http://localhost/assets/restaurant-icons/014-waitress.png"> Modificar Pedidos</h1>
	</div>

	<div  class="" id="detalles-pedido">
		<div class="table-responsive">
			<table class="table table-hover">
				<tr>
					<th>Nombre</th>
					<th>Descripción</th>
					<th>Cantidad</th>
					<th>Añadir</th>
					<th>Quitar</th>
				</tr>
				<tr>
					<td>Fanta</td>
					<td>Lata de fanta naranja normal 355ml</td>
					<td>2</td>
					<td>
						<button class="btn btn-success" data-id=""><b>+</b></button>
					</td>
					<td>
						<button class="btn btn-warning" data-id=""><b>-</b></button>
					</td>
				</tr>
			</table>
		</div>
		<button class="btn btn-success" data-id=""><span class="glyphicon glyphicon-plus"></span><b>&nbspAñadir producto</b></button>
	</div>
</div>







