<div class="row" id="subtipos">
	<?php
	foreach ($infoProductos as $subtipo) {
		?>
		<div class="col-xs-6 col-sm-3 text-center">
			<a href="#" data-id="<?php echo $subtipo['id']; ?>" class="subtipo">
				<img class="icon4"
					<?php
					if (file_exists('assets/images/productos/'.$subtipo["url_foto_subtipo"]) &&
						$subtipo["url_foto_subtipo"] != null){?>
						src="<?php echo base_url("assets/images/productos/".$subtipo["url_foto_subtipo"]);?>"
					<?php }else{ ?>
					 src="<?php echo base_url("assets/images/productos/default.png"); ?>"
					 <?php } ?>alt="Foto subtipo">
				<h4><?php echo $subtipo['nombre'];?></h4>
			</a>
		</div>
	<?php } ?>
</div>






