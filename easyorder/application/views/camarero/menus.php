<div class="row" id="info-menus">
	<?php
	foreach ($infoMenus as $menu) {
		?>
		<div class="col-xs-6 col-sm-3 text-center">
			<a href="#" data-id="<?php echo $menu['id']; ?>" class="menu">
				<img class="icon4"
					<?php
					if (file_exists('assets/images/menus/'.$menu["url_foto_menu"]) &&
						$menu["url_foto_menu"] != null){?>
						src="<?php echo base_url("assets/images/menus/".$menu["url_foto_menu"]);?>"
					<?php }else{ ?>
					 src="<?php echo base_url("assets/images/menus/default.png"); ?>"
					 <?php } ?>alt="Foto menu">
			</a>
			<h4><?php echo $menu['nombre'];?></h4>
		</div>
	<?php } ?>
</div>






