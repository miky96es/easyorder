<!--SIN ANIMACION-->
<div class="row " id="accordion">
	<div class="col-xs-12 col-sm-12 text-center entrantes">Entrantes</div>
	<div class="products col-xs-12 col-sm-12 text-center detalle-entrantes">
	<?php foreach ($infoProductos as $producto) {
		if($producto["id_tipo_producto"] == 3){?>
			<div class="col-xs-12 col-sm-4 text-center producto-menu" data-id="<?php echo $producto['id']; ?>">
				<a href="#">
					<img class="icon4"
					<?php
					if (file_exists('assets/images/productos/'.$producto["url_foto_producto"]) &&
						$producto["url_foto_producto"] != null){?>
							src="<?php echo base_url("assets/images/productos/".$producto["url_foto_producto"]);?>"
						<?php }else{ ?>
						 src="<?php echo base_url("assets/images/productos/default.png"); ?>"
						 <?php } ?>alt="Foto producto">
				</a>
				<h4 class="nombre-producto-menu"><?php echo $producto['nombre'];?></h4>
				<div class="info-producto-menu" id="<?php echo $producto['id']; ?>">
					<div id = "descripcion"><?php echo $producto['descripcion'];?></div>
					<div id = "precio"><?php echo $producto['precio'];?>€</div>
					<button class="btn btn-success btn-añadir-pedido" data-id="<?php echo $producto['id'];?>"><span class="glyphicon glyphicon-plus"></span><b>Añadir al pedido</b></button>
				</div>
			</div>
		<?php }
	} ?>
	</div>
	<div class="col-xs-12 col-sm-12 text-center p1">Primer plato</div>
	<div class="products col-xs-12 col-sm-12 text-center detalle-p1">
	<?php foreach ($infoProductos as $producto) {
		if($producto["id_tipo_producto"] == 1){?>
			<div class="col-xs-12 col-sm-4 text-center producto-menu" data-id="<?php echo $producto['id']; ?>">
				<a href="#">
					<img class="icon4"
						<?php
						if (file_exists('assets/images/productos/'.$producto["url_foto_producto"]) &&
						$producto["url_foto_producto"] != null){?>
							src="<?php echo base_url("assets/images/productos/".$producto["url_foto_producto"]);?>"
						<?php }else{ ?>
						 src="<?php echo base_url("assets/images/productos/default.png"); ?>"
						 <?php } ?>alt="Foto producto">
				</a>
				<h4 class="nombre-producto-menu"><?php echo $producto['nombre'];?></h4>
				<div class="info-producto-menu" id="<?php echo $producto['id']; ?>">
					<div id = "descripcion"><?php echo $producto['descripcion'];?></div>
					<div id = "precio"><?php echo $producto['precio'];?>€</div>
					<button class="btn btn-success btn-añadir-pedido" data-id="<?php echo $producto['id'];?>"><span class="glyphicon glyphicon-plus"></span><b>Añadir al pedido</b></button>
				</div>
			</div>
		<?php }
	} ?>
	</div>
	<div class="col-xs-12 col-sm-12 text-center p2">Segundo plato</div>
	<div class="products col-xs-12 col-sm-12 text-center detalle-p2">
	<?php foreach ($infoProductos as $producto) {
		if($producto["id_tipo_producto"] == 2){?>
			<div class="col-xs-12 col-sm-4 text-center producto-menu" data-id="<?php echo $producto['id']; ?>">
				<a href="#">
					<img class="icon4"
						<?php
						if (file_exists('assets/images/productos/'.$producto["url_foto_producto"]) &&
						$producto["url_foto_producto"] != null){?>
							src="<?php echo base_url("assets/images/productos/".$producto["url_foto_producto"]);?>"
						<?php }else{ ?>
						 src="<?php echo base_url("assets/images/productos/default.png"); ?>"
						 <?php } ?>alt="Foto producto">
				</a>
				<h4 class="nombre-producto-menu"><?php echo $producto['nombre'];?></h4>
				<div class="info-producto-menu" id="<?php echo $producto['id']; ?>">
					<div id = "descripcion"><?php echo $producto['descripcion'];?></div>
					<div id = "precio"><?php echo $producto['precio'];?>€</div>
					<button class="btn btn-success btn-añadir-pedido" data-id="<?php echo $producto['id'];?>"><span class="glyphicon glyphicon-plus"></span><b>Añadir al pedido</b></button>
				</div>
			</div>
		<?php }
	} ?>
	</div>
	<div class="col-xs-12 col-sm-12 text-center bebidas">Bebidas</div>
	<div class="products col-xs-12 col-sm-12 text-center detalle-bebidas">
	<?php foreach ($infoProductos as $producto) {
		if($producto["id_tipo_producto"] == 4){?>
			<div class="col-xs-12 col-sm-4 text-center producto-menu" data-id="<?php echo $producto['id']; ?>">
				<a href="#">
					<img class="icon4"
						<?php
						if (file_exists('assets/images/productos/'.$producto["url_foto_producto"]) &&
						$producto["url_foto_producto"] != null){?>
							src="<?php echo base_url("assets/images/productos/".$producto["url_foto_producto"]);?>"
						<?php }else{ ?>
						 src="<?php echo base_url("assets/images/productos/default.png"); ?>"
						 <?php } ?>alt="Foto producto">
				</a>
				<h4class="nombre-producto-menu"><?php echo $producto['nombre'];?></h4>
				<div class="info-producto-menu" id="<?php echo $producto['id']; ?>">
					<div id = "descripcion"><?php echo $producto['descripcion'];?></div>
					<div id = "precio"><?php echo $producto['precio'];?>€</div>
					<button class="btn btn-success btn-añadir-pedido" data-id="<?php echo $producto['id'];?>"><span class="glyphicon glyphicon-plus"></span><b>Añadir al pedido</b></button>
				</div>
			</div>
		<?php }
	} ?>
	</div>
	<div class="col-xs-12 col-sm-12 text-center postres">Postres</div>
	<div class="products col-xs-12 col-sm-12 text-center detalle-postres">
	<?php foreach ($infoProductos as $producto) {
		if($producto["id_tipo_producto"] == 5){?>
			<div class="col-xs-12 col-sm-4 text-center producto-menu" data-id="<?php echo $producto['id']; ?>">
				<a href="#">
					<img class="icon4"
						<?php
						if (file_exists('assets/images/productos/'.$producto["url_foto_producto"]) &&
						$producto["url_foto_producto"] != null){?>
							src="<?php echo base_url("assets/images/productos/".$producto["url_foto_producto"]);?>"
						<?php }else{ ?>
						 src="<?php echo base_url("assets/images/productos/default.png"); ?>"
						 <?php } ?>alt="Foto producto">
				</a>
				<h4 class="nombre-producto-menu"><?php echo $producto['nombre'];?></h4>
				<div class="info-producto-menu" id="<?php echo $producto['id']; ?>">
					<div id = "descripcion"><?php echo $producto['descripcion'];?></div>
					<div id = "precio"><?php echo $producto['precio'];?>€</div>
					<button class="btn btn-success btn-añadir-pedido" data-id="<?php echo $producto['id'];?>"><span class="glyphicon glyphicon-plus"></span><b>Añadir al pedido</b></button>
				</div>
			</div>
		<?php }
	} ?>
	</div>
</div>



