<!--SIN ANIMACION-->
<div class="row" id="info-menu">
	<?php
	foreach ($infoMenu as $menu) {
		?>
		<div id="menu-i" class="col-xs-4 col-sm-3 text-center">
			<a href="#" data-id="<?php echo $menu['id']; ?>" class="menu">
				<img class="icon4"
					<?php
					if (file_exists('assets/images/menus/'.$menu["url_foto_menu"]) &&
					$menu["url_foto_menu"] != null){?>
						src="<?php echo base_url("assets/images/menus/".$menu["url_foto_menu"]);?>"
					<?php }else{ ?>
					 src="<?php echo base_url("assets/images/menus/default.png"); ?>"
					 <?php } ?>alt="Foto menu">
			</a>
			<h4><?php echo $menu['nombre'];?></h4>
			<h6><?php echo $menu['descripcion'];?></h6>
			<h6><?php echo $menu['precio'];?></h6>
		</div>
		<div id="platos" class="col-xs-8 col-sm-9 text-center"></div>
	<?php } ?>
</div>




