<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $titulo; ?></title>
	<!--Logo-->
	<link rel="icon" type="image/png" href="<?php echo base_url("assets/images/logo/logo.png"); ?>">
	<!--google-->
	<link href="https://fonts.googleapis.com/css?family=Baloo|Hind|Lobster" rel="stylesheet">
	<!--Fontawesome-->
	<link rel="stylesheet" href="<?php echo base_url("assets/fontawesome/css/font-awesome.min.css");?>">
	<!--========================== Jq =========================-->
	<script src="<?php echo base_url("assets/js/jquery.js"); ?>"></script>
	<!--================ Bootstrap ============================-->
	<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js"); ?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
	<!--========================== JQ UI 1.12.1 ===================-->
	<script src="<?php echo base_url("assets/js/jquery-ui-1.12.1.custom/jquery-ui.min.js"); ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url("assets/js/jquery-ui-1.12.1.custom/jquery-ui.min.css");?>">
	<!--========================== Js =========================-->
	<script src="<?php echo base_url("assets/js/cocinero/cocinero.js");?>"></script>
	<!--========================== CSS =========================-->
	<link rel="stylesheet" href="<?php echo base_url("assets/css/cocinero/cocinero.css");?>">
</head>
<body>
