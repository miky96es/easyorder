<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $titulo; ?></title>
	<link rel="icon" type="image/png" href="<?php echo base_url("assets/images/logo/logo.png"); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/bootstrap/css/bootstrap.min.css"); ?>">
	<script src="<?php echo base_url("assets/js/jquery.js"); ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url("assets/fontawesome/css/font-awesome.min.css");?>">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/login/login.css");?>">
	<script src="<?php echo base_url("assets/js/login/login.js");?>"></script>
	<link href="https://fonts.googleapis.com/css?family=Hind|Lobster" rel="stylesheet">
</head>
<body>
