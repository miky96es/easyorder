<div id="background-content">
	<div class="container-fluid">
		<div class="text-center">
			<h1><img class="icon" alt="icono pedidos"
					 src="<?php echo base_url("assets/restaurant-icons/021-catering.png"); ?>"> LISTADO PEDIDOS:</h1>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<input type="text" class="form-control buscador" id="buscador-pedidos" placeholder="Buscar">
				</div><!--
				<div class="col-xs-12 col-md-6">
					<a href="#" class="btn btn-success btn-block"><span class="glyphicon glyphicon-plus"></span><b>
							Añadir
							pedido</b></a>
				</div>-->
			</div>
		</div>
		<div class="container-fluid target-margin">
			<div class="table-responsive">
				<table id="pedidos-table" class="table table-hover tablesorter">
					<thead class="color5_target">
					<tr>
						<th>ID</th>
						<th>Fecha</th>
						<th>Estado pago</th>
						<th>Mesa</th>
						<th>Realizado por</th>
						<!--						<th>Ver contenido</th>
                        -->                    </tr>
					</thead>
					<tbody class="color6_target ">
					<?php
					foreach ($pedidos as $pedido) {
						//var_dump($pedido);
						$foto = ($pedido['estado_pago'] == 1) ? base_url('assets/restaurant-icons/039-credit-card-red.png') : base_url('assets/restaurant-icons/038-credit-card-green.png');
						echo
							'<tr class="linea">
								<td><span>' . $pedido['id'] . '</span></td>
								<td><span>' . $pedido['fecha'] . '</span></td>
								<td><img class="img-rounded icon2" alt="estado pedido ' . $pedido['estado_pago'] . '" src="' . $foto . '"></td>
								<td><span>' . $pedido['id_mesa'] . '</span></td>
								<td><span>' . $pedido['apellidos_user'] . ', ' . $pedido['nombre_user'] . '</span></td>
							</tr>';
					}
/*					$guardar = '<td><a href="' . base_url("admin/edit_user/") . $pedido["id"] . '"><img alt="ver productos pedidos ' . $pedido["id"] . '" class="icon2" src="' . base_url('assets/restaurant-icons/037-binoculars.png') . '"></a></td>';*/
					?>
					</tbody>
				</table>
			</div>
			<?php //echo $this->pagination->create_links();?>
		</div>
	</div>
</div>
