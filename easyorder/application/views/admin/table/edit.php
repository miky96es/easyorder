<div id="background-content">
	<div class="container-fluid">
		<div id="content" class="container">
			<?php
			if ($editado) {
				?>
				<div class="alert alert-success">
					<strong>Bien!</strong> Se ha editado correctamente la mesa
				</div>
			<?php }
			?>
			<div class="container-fluid"><a href="<?php echo base_url('c_admin/list_tables'); ?>">
					<button id="return-list" class="btn pull-right"><img class="icon" alt="listado"
																		 src="<?php echo base_url("assets/restaurant-icons/043-tasks.png"); ?>">
						<b>Volver
							al listado</b></button>
				</a></div>
			<div class="text-center">
				<h1><img alt="icono mesas" class="icon"
						 src="<?php echo base_url("assets/restaurant-icons/020-table.png"); ?>"> EDITAR
					MESA <?php if ($existe) {
						echo 'Nº ' . $mesa['id'];
					} ?></h1>
			</div>
			<?php
			if ($existe) {
				echo form_open(base_url('c_admin/edit_table/' . $mesa['id']));
				?>
				<div class="form-group">
					<?php
					echo form_label('<img alt="icono mesas" class="icon"
					 src="' . base_url("assets/restaurant-icons/042-numbers.png") . '"> Número comesales: ', 'n_comensales') .
						form_input(array('type' => 'number', 'name' => 'n_comensales', 'placeholder' => 'Introduce el número de comensales', 'class' => 'form-control', 'min' => '1', 'max' => '30', 'value' => $mesa['num_comensales']));
					?>
				</div>
				<?php
				echo form_button(array('type' => 'submit', 'content' => '<b><i class="fa fa-pencil" aria-hidden="true"></i> Editar mesa </b>', 'class' => 'btn btn-success btn-block')) .
					form_close();
				?>
			<?php } else { ?>
				<div class="panel panel-danger">
					<div class="panel-heading"><img class="icon2" alt="icono error"
													src="<?php echo base_url("assets/restaurant-icons/047-error.png"); ?>">
						Error! , esta mesa no existe
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
