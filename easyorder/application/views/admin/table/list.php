<div id="delete-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><img class="icon" alt="alerta"
											 src="<?php echo base_url("assets/restaurant-icons/044-warning.png"); ?>">
					Eliminar mesa nº <span id="borrar"></span></h4>
			</div>
			<div class="modal-body">
				<p>¿Está seguro que desea eliminar la mesa?</p>
			</div>
			<div class="modal-footer">
				<?php
				$hidden = array('id' => null);
				echo form_open('c_admin/delete_table', '', $hidden)
					. form_button(array('type' => 'submit', 'class' => 'btn btn-warning', 'content' => 'Aceptar'))
					. '<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>'
					. form_close();
				?>
			</div>
		</div>
	</div>
</div>

<div id="background-content">
	<?php if (isset($_SESSION['id']) && isset($_SESSION['borrado'])) {
		if ($_SESSION['borrado'] == 0) {
			?>
			<div class="alert alert-danger alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> No se puede eliminar la mesa nº <?php echo $_SESSION['id'];?>, porque hay pedidos con esta mesa.
			</div>
			<?php
		} else {
			?>
			<div class="alert alert-success alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Bien!</strong> Se ha borrado correctamente la mesa nº <?php echo $_SESSION['id'];?>.
			</div>
			<?php
		}
	}
	?>
	<div class="container-fluid">
		<div class="text-center">
			<h1><img alt="icono mesas" class="icon"
					 src="<?php echo base_url("assets/restaurant-icons/020-table.png"); ?>"> LISTADO MESAS:</h1>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<input type="text" class="form-control buscador" id="buscador-tables" placeholder="Buscar">
				</div>
				<div class="col-xs-12 col-md-6">
					<a href="<?php echo base_url('c_admin/add_table'); ?>" class="btn btn-success btn-block"><span
							class="glyphicon glyphicon-plus"></span><b>
							Añadir
							mesa</b></a>
				</div>
			</div>
		</div>
		<div class="container-fluid target-margin">
			<div class="table-responsive">
				<table id="tables-table" class="table table-hover tablesorter">
					<thead class="color5_target">
					<tr>
						<th>ID</th>
						<th>Estado</th>
						<th>Número comensales</th>
						<th>Editar</th>
						<th>Borrar</th>
					</tr>
					</thead>
					<tbody class="color6_target ">
					<?php
					foreach ($mesas as $mesa) {
						$foto = ($mesa['estado'] == 1) ? base_url('assets/restaurant-icons/041-table-green.png') : base_url('assets/restaurant-icons/040-table-red.png');
						echo
							'<tr class="linea">
							<td><span>' . $mesa["id"] . '</span></td>
							<td><img class="img-rounded icon2" alt="estado pedido ' . $mesa['estado'] . '" src="' . $foto . '"></td>
							<td><span>' . $mesa["num_comensales"] . '</span></td>
							<td><a href="' . base_url("c_admin/edit_table/") . $mesa["id"] . '"><img alt="editar mesa ' . $mesa["id"] . '" class="icon2" src="' . base_url('assets/restaurant-icons/035-edit.png') . '"></a></td>
							<td><img alt="borrar mesa ' . $mesa["id"] . '" class="icon2 clicable borrable" data-id="' . $mesa['id'] . '" data-toggle="modal" data-target="#delete-modal" src="' . base_url('assets/restaurant-icons/034-delete.png') . '"></td>
						</tr>';
					}
					?>
					</tbody>
				</table>
			</div>
			<?php //echo $this->pagination->create_links();?>
		</div>
	</div>
</div>
