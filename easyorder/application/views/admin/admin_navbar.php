<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url("c_login"); ?>"><span id="title-brand" id="logo-span"><img class="navbar-icon" src="<?php echo base_url("assets/images/logo/logo.png"); ?>">
					EasyOrder</span>
			</a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu_desplegable"><span
					class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span
					class="icon-bar"></span>
			</button>
		</div>
		<div id="menu_desplegable" class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"><img class="navbar-icon img-circle"
									 src="<?php
									 echo $foto; ?>"
									 alt="foto perfil"> <?php echo $apellidos . ", " . $nombre; ?></a></li>
				<li><a href="<?php echo base_url("c_login/logout"); ?>">
						<img class="navbar-icon" src="<?php echo base_url('assets/restaurant-icons/036-exit.png');?>" alt="cerrar sesion">
						Cerrar sesión</a></li>
			</ul>
		</div>
	</div>
</nav>
