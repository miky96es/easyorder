<div id="delete-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><img class="icon" alt="alerta"
											 src="<?php echo base_url("assets/restaurant-icons/044-warning.png"); ?>">
					Eliminar usuario con id <span id="borrar"></span></h4>
			</div>
			<div class="modal-body">
				<p>¿Está seguro que desea eliminar el usuario?</p>
			</div>
			<div class="modal-footer">
				<?php
				$hidden = array('id' => null);
				echo form_open('c_admin/delete_user', '', $hidden)
					. form_button(array('type' => 'submit', 'class' => 'btn btn-warning', 'content' => 'Aceptar'))
					. '<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>'
					. form_close();
				?>
			</div>
		</div>
	</div>
</div>

<div id="background-content">
	<?php if (isset($_SESSION['id']) && isset($_SESSION['borrado'])) {
		if ($_SESSION['borrado'] == 0) {
			?>
			<div class="alert alert-danger alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> No se puede eliminar el usuario con id <?php echo $_SESSION['id']; ?>.
			</div>
			<?php
		} else {
			?>
			<div class="alert alert-success alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Bien!</strong> Se ha borrado correctamente el usuario con id <?php echo $_SESSION['id']; ?>.
			</div>
			<?php
		}
	}
	?>
	<div class="container-fluid">
		<div class="text-center">
			<h1><img class="icon" alt="icono camarera"
					 src="<?php echo base_url("assets/restaurant-icons/025-avatar.png"); ?>">
				<img
					class="icon" alt="icono camarero"
					src="<?php echo base_url("assets/restaurant-icons/026-waiter.png"); ?>"> LISTADO USUARIOS:</h1>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<input type="text" class="form-control buscador" id="buscador-users" placeholder="Buscar">
				</div>
				<div class="col-xs-12 col-md-6">
					<a href="<?php echo base_url('c_admin/add_user'); ?>" class="btn btn-success btn-block"><span
							class="glyphicon glyphicon-plus"></span><b>
							Añadir
							usuario</b></a>
				</div>
			</div>
		</div>
		<div class="container-fluid target-margin">
			<div class="table-responsive">
				<table id="users-table" class="table table-hover tablesorter">
					<thead class="color5_target">
					<tr>
						<th>ID</th>
						<th>Foto perfil</th>
						<th>Nombre</th>
						<th>Apellidos</th>
						<th>Móvil</th>
						<th>Cargo</th>
						<th>Email</th>
						<th>Fecha nacimiento</th>
						<th>Nacionalidad</th>
						<th>Sueldo</th>
						<th>Editar</th>
						<th>Borrar</th>
					</tr>
					</thead>
					<tbody class="color6_target ">
					<?php
					foreach ($usuarios as $usuario) {
						$cargo = '';
						if ($usuario['cargo'] == 1) {
							$cargo = 'Administrador';
						} else if ($usuario['cargo'] == 2) {
							$cargo = 'Cocinero';
						} else if ($usuario['cargo'] == 3) {
							$cargo = 'Camarero';
						}
						$foto = file_exists('files/user_files/images/' . $usuario['url_foto_usuario']) && !is_null($usuario['url_foto_usuario']) ? base_url('files/user_files/images/' . $usuario['url_foto_usuario']) : base_url('assets/restaurant-icons/032-user.png');
						echo
							'<tr class="linea">
								<td><span>' . $usuario['id'] . '</span></td>
								<td><img class="img-circle profile-image-list" alt="usuario' . $usuario['id'] . '" src="' . $foto . '"></td>
								<td><span>' . $usuario['nombre'] . '</span></td>
								<td><span>' . $usuario['apellidos'] . '</span></td>
								<td><span>' . $usuario['movil'] . '</span></td>
								<td><span>' . $cargo . '</span></td>
								<td><span>' . $usuario['email'] . '</span></td>
								<td><span>' . $usuario['fecha_nac'] . '</span></td>
								<td><span>' . $usuario['nacionalidad'] . '</span></td>
								<td><span>' . $usuario['sueldo'] . '&#8364;</span></td>
								<td><a href="' . base_url("c_admin/edit_user/") . $usuario["id"] . '"><img alt="editar usuario ' . $usuario["id"] . '" class="icon2" src="' . base_url('assets/restaurant-icons/035-edit.png') . '"></a></td>
								<td><img alt="borrar usuario ' . $usuario["id"] . '" class="icon2  clicable borrable" data-id="' . $usuario['id'] . '" data-toggle="modal" data-target="#delete-modal" src="' . base_url('assets/restaurant-icons/034-delete.png') . '"></td>
							</tr>';
					}
					?>
					</tbody>
				</table>
			</div>
			<?php //echo $this->pagination->create_links();?>
		</div>
	</div>
</div>
