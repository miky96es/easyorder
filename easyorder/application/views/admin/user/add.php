<div id="background-content">
	<div class="container-fluid">
		<div id="content" class="container">
			<?php
			if (isset($_SESSION['dni_creado']) && isset($_SESSION['creado'])) {
				if ($_SESSION['creado']) {
					?>
					<div class="alert alert-success">
						<strong>Bien!</strong> Se ha añadido correctamente el
						usuario <?php echo $_SESSION['dni_creado']; ?>
					</div>
					<?php
				}
			} else if (isset($_SESSION['creado'])) {
				if (!$_SESSION['creado']) {
					?>
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha añadido el usuario, no se ha podido subir la imagen (Solo se permite formato PNG y JPG).
					</div>
					<?php
				}
			}
			?>

			<div class="container-fluid"><a href="<?php echo base_url('c_admin/list_users'); ?>">
					<button id="return-list" class="btn pull-right"><img class="icon" alt="listado"
																		 src="<?php echo base_url("assets/restaurant-icons/043-tasks.png"); ?>">
						<b>Volver
							al listado</b></button>
				</a></div>
			<?php
			if (validation_errors()) {
				echo "<div id='errors' class=\"alert alert-danger\">" . validation_errors() . "</div>";
			}
			?>
			<div class="text-center">
				<h1><img class="icon" alt="icono camarera"
						 src="<?php echo base_url("assets/restaurant-icons/025-avatar.png"); ?>">
					<img
						class="icon" alt="icono camarero"
						src="<?php echo base_url("assets/restaurant-icons/026-waiter.png"); ?>"> NUEVO USUARIO</h1>
			</div>
			<?php
			echo form_open_multipart(base_url('c_admin/add_user'));
			?>
			<div class="form-group">
				<?php
				echo form_label('DNI: ', 'id') .
					form_input(array('type' => 'text', 'name' => 'id', 'placeholder' => 'Introduce el dni', 'class' => 'form-control', 'maxlength' => '9', 'minlength' => '9', 'value' => set_value('id')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Nombre: ', 'nombre') .
					form_input(array('type' => 'text', 'name' => 'nombre', 'placeholder' => 'Introduce el nombre', 'class' => 'form-control', 'maxlength' => '50', 'value' => set_value('nombre')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Apellidos: ', 'apellidos') .
					form_input(array('type' => 'text', 'name' => 'apellidos', 'placeholder' => 'Introduce los apellidos', 'class' => 'form-control', 'maxlength' => '100', 'value' => set_value('apellidos')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Móvil: ', 'movil') .
					form_input(array('type' => 'text', 'name' => 'movil', 'placeholder' => 'Introduce el nº de móvil', 'class' => 'form-control','maxlength' => '14', 'value' => set_value('movil')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Cargo: ', 'cargo');
				?>
				<br>
				<label class="radio-inline">
					<?php
					echo form_radio(array('name' => 'cargo'), '1', (set_value('cargo') == 1) ? "checked" : "");
					?>
					Administrador
				</label>
				<label class="radio-inline">
					<?php
					echo form_radio(array('name' => 'cargo'), '2', (set_value('cargo') == 2) ? "checked" : "");
					?>
					Cocinero

				</label>
				<label class="radio-inline">
					<?php
					echo form_radio(array('name' => 'cargo'), '3', (set_value('cargo') == 3) ? "checked" : "");
					?>
					Camarero
				</label>

			</div>
			<div class="form-group">
				<?php
				echo form_label('Contraseña: ', 'pass') .
					form_input(array('type' => 'text', 'name' => 'pass', 'placeholder' => 'Introduce la contraseña', 'class' => 'form-control', 'maxlength' => '50', 'value' => set_value('pass')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Email: ', 'email') .
					form_input(array('type' => 'email', 'name' => 'email', 'placeholder' => 'Introduce el email', 'class' => 'form-control', 'maxlength' => '150','value' => set_value('email')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Fecha nacimiento: ', 'fecha_nac') .
					form_input(array('type' => 'date', 'name' => 'fecha_nac', 'class' => 'form-control', 'max' => '1998-01-01', 'value' => set_value('fecha_nac')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Nacionalidad: ', 'nacionalidad') .
					form_input(array('type' => 'text', 'name' => 'nacionalidad', 'placeholder' => 'Introduce la nacionalidad', 'class' => 'form-control', 'maxlength' => '50', 'value' => set_value('nacionalidad')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Sueldo: ', 'sueldo') .
					form_input(array('type' => 'number', 'name' => 'sueldo', 'placeholder' => 'Introduce el sueldo', 'class' => 'form-control', 'min' => '300.00', 'step'=>'0.01', 'max' => '9999.99', 'value' => set_value('sueldo')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Foto: ', 'url_foto_usuario') .
					form_input(array('type' => 'file', 'class'=>'form-control','name' => 'url_foto_usuario'));
				?>
			</div>
			<?php
			echo form_button(array('type' => 'submit', 'content' => '<b><i class="fa fa-plus" aria-hidden="true"></i>Añadir usuario</b>', 'class' => 'btn btn-success btn-block')) .
				form_close();
			?>
		</div>
	</div>
</div>
