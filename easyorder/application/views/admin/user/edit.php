<div id="background-content">
	<div class="container-fluid">
		<div id="content" class="container">
			<?php if (validation_errors() == false) {
				if ($editado == true && $enviado == true) {
					?>
					<div class="alert alert-success">
						<strong>Bien!</strong> Se ha editado correctamente el usuario
					</div>
				<?php }
				if ($editado == false && $enviado == true) {
					?>
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha editado el usuario, no se ha podido subir la imagen (Solo
						se
						permite formato PNG y JPG).
					</div>
					<?php
				}
			}
			?>
			<div class="container-fluid"><a href="<?php echo base_url('c_admin/list_users'); ?>">
					<button id="return-list" class="btn pull-right"><img class="icon" alt="listado"
																		 src="<?php echo base_url("assets/restaurant-icons/043-tasks.png"); ?>">
						<b>Volver
							al listado</b></button>
				</a></div>
			<?php
			if (validation_errors()) {
				echo "<div id='errors' class=\"alert alert-danger\">" . validation_errors() . "</div>";
			}
			?>
			<div class="text-center">
				<h1><img class="icon" alt="icono camarera"
						 src="<?php echo base_url("assets/restaurant-icons/025-avatar.png"); ?>">
					<img
						class="icon" alt="icono camarero"
						src="<?php echo base_url("assets/restaurant-icons/026-waiter.png"); ?>"> EDITAR
					USUARIO <?php if ($existe) {
						echo $usuario['id'];
					} ?></h1>
			</div>
			<?php
			if ($existe) {
				echo form_open_multipart(base_url('c_admin/edit_user/' . $usuario['id']));
				?>
				<div class="form-group">
					<?php
					echo form_label('Nombre: ', 'nombre') .
						form_input(array('type' => 'text', 'name' => 'nombre', 'placeholder' => 'Introduce el nombre', 'class' => 'form-control', 'maxlength' => '50', 'value' => $usuario['nombre']));
					?>
				</div>
				<div class="form-group">
					<?php
					echo form_label('Apellidos: ', 'apellidos') .
						form_input(array('type' => 'text', 'name' => 'apellidos', 'placeholder' => 'Introduce los apellidos', 'class' => 'form-control', 'maxlength' => '100', 'value' => $usuario['apellidos']));
					?>
				</div>
				<div class="form-group">
					<?php
					echo form_label('Móvil: ', 'movil') .
						form_input(array('type' => 'text', 'name' => 'movil', 'placeholder' => 'Introduce el nº de móvil', 'class' => 'form-control', 'maxlength' => '14', 'value' => $usuario['movil']));
					?>
				</div>
				<div class="form-group">
					<?php
					echo form_label('Cargo: ', 'cargo');
					?>
					<br>
					<label class="radio-inline">
						<?php
						echo form_radio(array('name' => 'cargo'), '1', (($usuario['cargo'] == 1) ? true : false));
						?>
						Administrador
					</label>
					<label class="radio-inline">
						<?php
						echo form_radio(array('name' => 'cargo'), '2', (($usuario['cargo'] == 2) ? true : false));
						?>
						Cocinero

					</label>
					<label class="radio-inline">
						<?php
						echo form_radio(array('name' => 'cargo'), '3', (($usuario['cargo'] == 3) ? true : false));
						?>
						Camarero
					</label>

				</div>
				<div class="form-group">
					<?php
					echo form_label('Contraseña: ', 'pass') .
						form_input(array('type' => 'text', 'name' => 'pass', 'placeholder' => 'Introduce la contraseña', 'class' => 'form-control', 'maxlength' => '50', 'value' => $usuario['pass']));
					?>
				</div>
				<div class="form-group">
					<?php
					echo form_label('Email: ', 'email') .
						form_input(array('type' => 'email', 'name' => 'email', 'placeholder' => 'Introduce el email', 'maxlength' => '150', 'class' => 'form-control', 'value' => $usuario['email']));
					?>
				</div>
				<div class="form-group">
					<?php
					echo form_label('Fecha nacimiento: ', 'fecha_nac') .
						form_input(array('type' => 'date', 'name' => 'fecha_nac', 'class' => 'form-control', 'max' => '1998-01-01', 'value' => $usuario['fecha_nac']));
					?>
				</div>
				<div class="form-group">
					<?php
					echo form_label('Nacionalidad: ', 'nacionalidad') .
						form_input(array('type' => 'text', 'name' => 'nacionalidad', 'maxlength' => '50', 'placeholder' => 'Introduce la nacionalidad', 'class' => 'form-control', 'value' => $usuario['nacionalidad']));
					?>
				</div>
				<div class="form-group">
					<?php
					echo form_label('Sueldo: ', 'sueldo') .
						form_input(array('type' => 'number', 'name' => 'sueldo', 'step' => '0.01', 'placeholder' => 'Introduce el sueldo', 'class' => 'form-control', 'min' => '300.00', 'max' => '9999.99', 'value' => $usuario['sueldo']));
					?>
				</div>
				<div class="form-group">
					<?php
					echo form_label('Foto: ', 'url_foto_usuario') .
						form_input(array('type' => 'file', 'name' => 'url_foto_usuario', 'class' => 'form-control'));
					?>
				</div>
				<?php
				echo form_button(array('type' => 'submit', 'content' => '<b><i class="fa fa-pencil" aria-hidden="true"></i> Editar usuario</b>', 'class' => 'btn btn-success btn-block')) .
					form_close();
				?>
			<?php } else { ?>
				<div class="panel panel-danger">
					<div class="panel-heading"><img class="icon2" alt="icono error"
													src="<?php echo base_url("assets/restaurant-icons/047-error.png"); ?>">
						Error! , este usuario no existe
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
