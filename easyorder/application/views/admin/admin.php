<div id="background-content">
	<div class="container text-center">
		<h1><img class="icon" alt="icono panel de control"
				 src="<?php echo base_url("assets/restaurant-icons/028-control-panel.png"); ?>"> PANEL DE CONTROL</h1>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-6 target-margin">
				<a href="<?php echo base_url("c_admin/list_tables"); ?>">
					<div class="color5_target">
						<div class="container-fluid text-center">
							<h3><img alt="icono mesas" class="icon"
									 src="<?php echo base_url("assets/restaurant-icons/020-table.png"); ?>"> MESAS
							</h3>
						</div>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 target-margin">
				<a href="<?php echo base_url("c_admin/list_users"); ?>">
					<div class="color5_target">
						<div class="container-fluid text-center">
							<h3><img class="icon" alt="icono camarera"
									 src="<?php echo base_url("assets/restaurant-icons/025-avatar.png"); ?>"> <img
									class="icon" alt="icono camarero"
									src="<?php echo base_url("assets/restaurant-icons/026-waiter.png"); ?>">
								USUARIOS</h3>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 target-margin">
				<a href="<?php echo base_url("c_admin/list_products"); ?>">
					<div class="color5_target">
						<div class="container-fluid text-center">
							<h3><img class="icon" alt="icono productos"
									 src="<?php echo base_url("assets/restaurant-icons/007-tray-1.png"); ?>">
								PRODUCTOS
							</h3>
						</div>
					</div>
				</a>
			</div>
			<div class="col-xs-12 col-sm-6 target-margin">
				<a href="<?php echo base_url("c_admin/list_menus"); ?>">
					<div class="color5_target">
						<div class="container-fluid text-center">
							<h3><img class="icon" alt="icono menús"
									 src="<?php echo base_url("assets/restaurant-icons/029-restaurant.png"); ?>">
								MENÚS</h3>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 target-margin">
				<a href="<?php echo base_url("c_admin/list_orders"); ?>">
					<div class="color5_target">
						<div class="container-fluid text-center">
							<h3><img class="icon" alt="icono pedidos"
									 src="<?php echo base_url("assets/restaurant-icons/021-catering.png"); ?>">
								PEDIDOS
							</h3>
						</div>
					</div>
				</a>
			</div>
			<!--<div class="col-xs-12 col-sm-6 target-margin">
				<a href="<?php /*echo base_url("c_admin/statistics"); */?>">
					<div class="color5_target">
						<div class="container-fluid text-center">
							<h3><img class="icon" alt="estadísticas"
									 src="<?php /*echo base_url("assets/restaurant-icons/030-presentation.png"); */?>">
								ESTADÍSTICAS</h3>
						</div>
					</div>
				</a>
			</div>-->
		</div>
		<div class="row">
			<?php
			/*echo '
				<div class="col-xs-12 col-sm-6 target-margin">
					<a href="<?php echo base_url("c_admin/list_reservations"); ?>">
						<div class="color5_target">
							<div class="container-fluid text-center">
								<h3><img class="icon" alt="icono reservas"
										 src="<?php echo base_url("assets/restaurant-icons/031-reserved.png"); ?>">
									RESERVAS</h3>
							</div>
						</div>
					</a>
				</div>
				';*/
			?>
		</div>

		<br>
	</div>
</div>
