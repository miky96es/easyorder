<div id="background-content">
	<div class="container-fluid">
		<div id="content" class="container">
			<?php if (validation_errors() == false) {
				if ($editado == true && $enviado == true) {
					?>
					<div class="alert alert-success">
						<strong>Bien!</strong> Se ha editado correctamente el producto
					</div>
				<?php }
				if ($editado == false && $enviado == true) {
					?>
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha editado el producto, no se ha podido subir la imagen (Solo
						se
						permite formato PNG y JPG).
					</div>
					<?php
				}
			}
			?>
			<div class="container-fluid"><a href="<?php echo base_url('c_admin/list_products'); ?>">
					<button id="return-list" class="btn pull-right"><img class="icon" alt="listado"
																		 src="<?php echo base_url("assets/restaurant-icons/043-tasks.png"); ?>">
						<b>Volver
							al listado</b></button>
				</a></div>
			<?php
			if (validation_errors()) {
				echo "<div id='errors' class=\"alert alert-danger\">" . validation_errors() . "</div>";
			}
			?>
			<div class="text-center">
				<h1><img class="icon" alt="icono producto"
						 src="<?php echo base_url("assets/restaurant-icons/007-tray-1.png"); ?>">
					EDITAR PRODUCTO Nº<?php if ($existe) {
						echo $producto['id'];
					} ?></h1>
			</div>
			<?php
			if ($existe) {
			echo form_open_multipart(base_url('c_admin/edit_product/'.$producto['id']));
			?>
			<div class="form-group">
				<?php
				echo form_label('Nombre: ', 'nombre') .
					form_input(array('type' => 'text', 'name' => 'nombre', 'placeholder' => 'Introduce el nombre', 'class' => 'form-control', 'maxlength' => '100',  'value' => $producto['nombre']));
				?>
			</div>

			<div class="form-group">
				<?php
				echo form_label('Descripción: ', 'descripcion') .
					form_textarea(array('name' => 'descripcion', 'placeholder' => 'Introduce una descripción', 'class' => 'form-control', 'rows' => '3', 'maxlength' => '200', 'value' => $producto['descripcion']));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Precio: ', 'precio') .
					form_input(array('type' => 'number', 'class' => 'form-control', 'name' => 'precio', 'max' => '999.99', 'min' => '0.00', 'step' => '0.01', 'value' => $producto['precio']));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Tipo: ', 'tipo') . '<br>' .
					form_dropdown('tipo', $tipos_to_form, '', array('class' => 'form-control', 'id' => 'tipos_select'));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Subtipo: ', 'id_subtipo_producto');
				?>
				<select class="form-control" name="id_subtipo_producto">
					<?php
					foreach ($subtipos as $subtipo) {
						?>
						<option class="tipos-mostrar tipo-<?php echo $subtipo['id_tipo_producto']; ?>"
								value="<?php echo $subtipo['id']; ?>"><?php echo $subtipo['nombre']; ?></option>
						<?php
					}
					?>
				</select>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Foto: ', 'url_foto_producto') .
					form_input(array('type' => 'file', 'class' => 'form-control', 'name' => 'url_foto_producto'));
				?>
			</div>
			<?php
			echo form_button(array('type' => 'submit', 'content' => '<b><i class="fa fa-pencil" aria-hidden="true"></i>Editar producto</b>', 'class' => 'btn btn-success btn-block')) .
				form_close();
			?>
			<?php } else { ?>
				<div class="panel panel-danger">
					<div class="panel-heading"><img class="icon2" alt="icono error"
													src="<?php echo base_url("assets/restaurant-icons/047-error.png"); ?>">
						Error! , este producto no existe
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
