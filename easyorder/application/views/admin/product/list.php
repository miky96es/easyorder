<div id="delete-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><img class="icon" alt="alerta"
											 src="<?php echo base_url("assets/restaurant-icons/044-warning.png"); ?>">
					Eliminar el producto con id <span id="borrar"></span></h4>
			</div>
			<div class="modal-body">
				<p>¿Está seguro que desea eliminar el producto?</p>
			</div>
			<div class="modal-footer">
				<?php
				$hidden = array('id' => null);
				echo form_open('c_admin/delete_product', '', $hidden)
					. form_button(array('type' => 'submit', 'class' => 'btn btn-warning', 'content' => 'Aceptar'))
					. '<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>'
					. form_close();
				?>
			</div>
		</div>
	</div>
</div>

<div id="background-content">
	<?php if (isset($_SESSION['id']) && isset($_SESSION['borrado'])) {
		if ($_SESSION['borrado'] == 0) {
			?>
			<div class="alert alert-danger alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> No se puede eliminar el producto con id <?php echo $_SESSION['id']; ?>.
			</div>
			<?php
		} else {
			?>
			<div class="alert alert-success alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Bien!</strong> Se ha borrado correctamente el producto con id <?php echo $_SESSION['id']; ?>.
			</div>
			<?php
		}
	}
	?>
	<div class="container-fluid">
		<div class="text-center">
			<h1><img class="icon" alt="icono productos"
					 src="<?php echo base_url("assets/restaurant-icons/007-tray-1.png"); ?>"> LISTADO PRODUCTOS:</h1>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<input type="text" class="form-control buscador" id="buscador-products" placeholder="Buscar">
				</div>
				<div class="col-xs-12 col-md-6">
					<a href="<?php echo base_url('c_admin/add_product');?>" class="btn btn-success btn-block"><span class="glyphicon glyphicon-plus"></span><b>
							Añadir
							producto</b></a>
				</div>
			</div>
		</div>
		<div class="container-fluid target-margin">
			<div class="table-responsive">
				<table id="products-table" class="table table-hover tablesorter">
					<thead class="color5_target">
					<tr>
						<th>ID</th>
						<th>Imagen producto</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Precio</th>
						<th>Tipo</th>
						<th>Subtipo</th>
						<th>Editar</th>
						<th>Borrar</th>
					</tr>
					</thead>
					<tbody class="color6_target ">
					<?php
					foreach ($productos as $producto) {
						//var_dump($producto);
						$foto = file_exists('assets/images/productos/' . $producto['url_foto_producto']) && !is_null($producto['url_foto_producto']) ? base_url('assets/images/productos/' . $producto['url_foto_producto']) : base_url('assets/restaurant-icons/007-tray-1.png');
						echo
							'<tr class="linea">
								<td><span>' . $producto['id'] . '</span></td>
								<td><img class="img-rounded profile-image-list" alt="producto' . $producto['id'] . '" src="' . $foto . '"></td>
								<td class="text-left"><span>' . $producto['nombre'] . '</span></td>
								<td class="text-left"><span>' . $producto['descripcion'] . '</span></td>
								<td><span>' . $producto['precio'] . '&#8364;</span></td>
								<td><span>' . $producto['tipo'] . '</span></td>
								<td><span>' . $producto['subtipo'] . '</span></td>
								<td><a href="' . base_url("c_admin/edit_product/") . $producto["id"] . '"><img alt="editar producto ' . $producto["id"] . '" class="icon2" src="' . base_url('assets/restaurant-icons/035-edit.png') . '"></a></td>
								<td><img alt="borrar producto ' . $producto["id"] . '" class="icon2  clicable borrable" data-id="' . $producto['id'] . '" data-toggle="modal" data-target="#delete-modal" src="' . base_url('assets/restaurant-icons/034-delete.png') . '"></td>
							</tr>';
					}
					?>
					</tbody>
				</table>
			</div>
			<?php //echo $this->pagination->create_links();?>
		</div>
	</div>
</div>
