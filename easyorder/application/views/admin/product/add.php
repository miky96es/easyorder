<div id="background-content">
	<div class="container-fluid">
		<div id="content" class="container">
			<?php
			if (isset($_SESSION['nombre_prod']) && isset($_SESSION['creado'])) {
				if ($_SESSION['creado']) {
					?>
					<div class="alert alert-success">
						<strong>Bien!</strong> Se ha añadido correctamente el
						producto <?php echo $_SESSION['nombre_prod']; ?>
					</div>
					<?php
				}
			} else if (isset($_SESSION['creado'])) {
				if (!$_SESSION['creado']) {
					?>
					<div class="alert alert-danger">
						<strong>Error!</strong> No se ha añadido el producto
					</div>
					<?php
				}
			}
			?>
			<div class="container-fluid"><a href="<?php echo base_url('c_admin/list_products'); ?>">
					<button id="return-list" class="btn pull-right"><img class="icon" alt="listado"
																		 src="<?php echo base_url("assets/restaurant-icons/043-tasks.png"); ?>">
						<b>Volver
							al listado</b></button>
				</a></div>
			<?php
			if (validation_errors()) {
				echo "<div id='errors' class=\"alert alert-danger\">" . validation_errors() . "</div>";
			}
			?>
			<div class="text-center">
				<h1><img class="icon" alt="icono producto"
						 src="<?php echo base_url("assets/restaurant-icons/007-tray-1.png"); ?>">
					NUEVO PRODUCTO</h1>
			</div>
			<?php
			echo form_open_multipart(base_url('c_admin/add_product'));
			?>
			<div class="form-group">
				<?php
				echo form_label('Nombre: ', 'nombre') .
					form_input(array('type' => 'text', 'name' => 'nombre', 'placeholder' => 'Introduce el nombre', 'class' => 'form-control', 'maxlength' => '100', 'value' => set_value('nombre')));
				?>
			</div>

			<div class="form-group">
				<?php
				echo form_label('Descripción: ', 'descripcion') .
					form_textarea(array('name' => 'descripcion', 'placeholder' => 'Introduce una descripción', 'class' => 'form-control', 'rows' => '3', 'maxlength' => '200', 'value' => set_value('descripcion')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Precio: ', 'precio') .
					form_input(array('type' => 'number', 'class' => 'form-control', 'name' => 'precio', 'max' => '999.99', 'min' => '0.00', 'step' => '0.01', 'value' => set_value('precio')));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Tipo: ', 'tipo') . '<br>' .
					form_dropdown('tipo', $tipos_to_form, '', array('class' => 'form-control', 'id' => 'tipos_select'));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Subtipo: ', 'id_subtipo_producto');
				?>
				<select class="form-control" name="id_subtipo_producto">
					<?php
					foreach ($subtipos as $subtipo) {
						?>
						<option class="tipos-mostrar tipo-<?php echo $subtipo['id_tipo_producto']; ?>"
								value="<?php echo $subtipo['id']; ?>"><?php echo $subtipo['nombre']; ?></option>
						<?php
					}
					?>
				</select>
			</div>
			<div class="form-group">
				<?php
				echo form_label('Foto: ', 'url_foto_producto') .
					form_input(array('type' => 'file', 'class' => 'form-control', 'name' => 'url_foto_producto'));
				?>
			</div>
			<?php
			echo form_button(array('type' => 'submit', 'content' => '<b><i class="fa fa-plus" aria-hidden="true"></i>Añadir producto</b>', 'class' => 'btn btn-success btn-block')) .
				form_close();
			?>
		</div>
	</div>
</div>
