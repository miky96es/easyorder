<div id="delete-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><img class="icon" alt="alerta"
											 src="<?php echo base_url("assets/restaurant-icons/044-warning.png"); ?>">
					Eliminar menú con id <span id="borrar"></span></h4>
			</div>
			<div class="modal-body">
				<p>¿Está seguro que desea eliminar el menú?</p>
			</div>
			<div class="modal-footer">
				<?php
				$hidden = array('id' => null);
				echo form_open('c_admin/delete_menu', '', $hidden)
					. form_button(array('type' => 'submit', 'class' => 'btn btn-warning', 'content' => 'Aceptar'))
					. '<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>'
					. form_close();
				?>
			</div>
		</div>
	</div>
</div>

<div id="background-content">
	<?php if (isset($_SESSION['id']) && isset($_SESSION['borrado'])) {
		if ($_SESSION['borrado'] == 0) {
			?>
			<div class="alert alert-danger alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> No se puede eliminar el menú con id <?php echo $_SESSION['id']; ?>.
			</div>
			<?php
		} else {
			?>
			<div class="alert alert-success alert-dismissable fade in">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Bien!</strong> Se ha borrado correctamente el menú con id <?php echo $_SESSION['id']; ?>.
			</div>
			<?php
		}
	}
	?>
	<div class="container-fluid">
		<div class="text-center">
			<h1><img class="icon" alt="icono menus"
					 src="<?php echo base_url("assets/restaurant-icons/029-restaurant.png"); ?>"> LISTADO MENÚS:</h1>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<input type="text" class="form-control buscador" id="buscador-menus" placeholder="Buscar">
				</div>
				<!--<div class="col-xs-12 col-md-6">
					<a href="#" class="btn btn-success btn-block"><span class="glyphicon glyphicon-plus"></span><b>
							Añadir
							menú</b></a>
				</div>-->
			</div>
		</div>
		<div class="container-fluid target-margin">
			<div class="table-responsive">
				<table id="menus-table" class="table table-hover tablesorter">
					<thead class="color5_target">
					<tr>
						<th>ID</th>
						<th>Imagen menú</th>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Precio</th>
						<!--<th>Ver contenido</th>
						<th>Editar</th>
						<th>Borrar</th>-->
					</tr>
					</thead>
					<tbody class="color6_target ">
					<?php
					foreach ($menus as $menu) {
						//var_dump($menu);
						$foto = file_exists('files/menus_files/images/' . $menu['url_foto_menu']) && !is_null($menu['url_foto_menu']) ? base_url('files/menus_files/images/' . $menu['url_foto_menu']) : base_url('assets/restaurant-icons/029-restaurant.png"');
						echo
							'<tr class="linea">
								<td><span>' . $menu['id'] . '</span></td>
								<td><img class="img-rounded profile-image-list" alt="menu' . $menu['id'] . '" src="' . $foto . '"></td>
								<td class="text-left"><span>' . $menu['nombre'] . '</span></td>
								<td class="text-left"><span>' . $menu['descripcion'] . '</span></td>
								<td><span>' . $menu['precio'] . '&#8364;</span></td>
								
							</tr>';
					}
					/*$guardar='<td><a href="' . base_url("admin/edit_user/") . $menu["id"] . '"><img alt="ver menus menu ' . $menu["id"] . '" class="icon2" src="' . base_url('assets/restaurant-icons/037-binoculars.png') . '"></a></td>
								<td><a href="' . base_url("admin/edit_user/") . $menu["id"] . '"><img alt="editar menu ' . $menu["id"] . '" class="icon2" src="' . base_url('assets/restaurant-icons/035-edit.png') . '"></a></td>
								<td><img alt="borrar menu ' . $menu["id"] . '" class="icon2  clicable borrable" data-id="' . $menu['id'] . '" data-toggle="modal" data-target="#delete-modal" src="' . base_url('assets/restaurant-icons/034-delete.png') . '"></td>';*/
					?>
					</tbody>
				</table>
			</div>
			<?php //echo $this->pagination->create_links();?>
		</div>
	</div>
</div>
