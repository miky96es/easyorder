<div id="background-content">
	<div class="text-center" id="titulo">
		<h1><img class="icon1" alt="icono pedidos"
				 src="http://localhost/assets/restaurant-icons/015-chef.png"> PEDIDOS
	</div>
	<div class="container-fluid">
		<div class="row" id="listado" style="background: white;">
			<div class="col-xs-6 col-sm-6 text-center">
				<h1>Cola</h1>
				<div class="table-responsive">
					<table class="table cola" id="tabla">
						<thead>
						<tr>
							<th><h4>Hora</h4></th>
							<th><h4>Nombre</h4></th>
							<th id="desc"><h4>Descripción</h4></th>
							<th><h4>Estado</h4></th>
							<th><h4>Acción</h4></th>
						</tr>
						</thead>
						<tbody>
						<?php
						foreach ($cola as $listado_cola) {
							?>
							<tr data-status="<?= $listado_cola['estado_producto'] ?>">
								<td><?= $listado_cola['fecha'] ?></td>
								<td><?= $listado_cola['nombre'] ?></td>
								<td width="200px">
									<?= $listado_cola['descripcion'] ?>
								</td>
								<td><?= $listado_cola['estado_producto'] == 1 ? 'En Cola' : 'En Preparación' ?></td>
								<td>
									<div class="btn-group">
										<?php if ($listado_cola['estado_producto'] == 1) { ?>
											<button class="btn btn-success btn-sm btn-move"
													data-id="<?= $listado_cola['id'] ?>">Mover a Preparación
											</button>
										<?php } else { ?>
											<button class="btn btn-success btn-sm btn-ready"
													data-id="<?= $listado_cola['id'] ?>">Listo
											</button>
											<button class="btn btn-warning btn-sm btn-return"
													data-id="<?= $listado_cola['id'] ?>">Devolver a Cola
											</button>
										<?php } ?>
									</div>
								</td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
				</div>
			</div>

			<div class="col-xs-6 col-sm-6 text-center">
				<h1>Listos</h1>
				<div class="table-responsive">
					<table class="table listos" id="tabla">
						<thead>
						<tr>
							<th><h4>Hora</h4></th>
							<th><h4>Nombre</h4></th>
							<th><h4>Pedido</h4></th>
							<th><h4>Acción</h4></th>
						</tr>
						</thead>
						<tbody>
						<?php
						foreach ($listos as $listado_listos) {
							?>
							<tr data-status="<?= $listado_listos['estado_producto'] ?>">
								<td><?= $listado_listos['fecha'] ?></td>
								<td><?= $listado_listos['nombre'] ?></td>
								<td>#<?= $listado_listos['id_pedido'] ?></td>
								<td>
									<div class="btn-group">
										<button class="btn btn-danger btn-sm btn-delivered"
												data-id="<?= $listado_listos['id'] ?>">Liberar
										</button>
										<button class="btn btn-info btn-sm btn-return"
												data-id="<?= $listado_listos['id'] ?>">
											Devolver a Cola
										</button>
									</div>
								</td>
							</tr>
							<?php
						}
						?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
