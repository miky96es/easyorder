<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url("C_login"); ?>"><img id="logo-navbar"
																				 src="<?php echo base_url("assets/images/logo/logo.png"); ?>"></a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu_desplegable"><span
					class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span
					class="icon-bar"></span>
			</button>
		</div>
		<div id="menu_desplegable" class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#"><img class="img-circle img-responsive" id="profile-image"
									 src="<?php $foto = is_null($foto)?base_url('assets/restaurant-icons/032-user.png'): base_url($foto);
									 echo $foto;?>"
									 alt="foto perfil"> <?php echo $apellidos . ", " . $nombre; ?></a></li>
				<li><a href="<?php echo base_url("C_login/logout"); ?>"><span class="glyphicon glyphicon-log-out"></span>
						Cerrar sesión</a></li>
			</ul>
		</div>
	</div>
</nav>
