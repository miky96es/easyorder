<!--<div class="container">
	<button class="btn btn-default" id="button_fscreen" onclick="toggleFullScreen()"><span><i class="fa fa-desktop"
																							  aria-hidden="true"> <b>Activar/Desactivar pantalla completa</b></i></span>
	</button>
</div>-->
<div class="vertical-center">
	<div id="content" class="container">
		<div>
			<?php
			echo form_open('c_login/enter');
			?>
			<div class="text-center">
				<h1><img class="icon" alt="logo easyorder" src="<?php echo base_url('assets/images/logo/logo2.png')?>"> <span class="easyorder">EasyOrder</span></h1>
			</div>
			<div class="form-group">
				<?php
				echo form_input(array('name' => 'dni', 'placeholder' => 'Introduce el dni', 'class' => 'form-control', 'id'=>'input-icon1'));
				?>
			</div>
			<div class="form-group">
				<?php
				echo form_password(array('name' => 'password', 'placeholder' => 'Introduce la contraseña', 'class' => 'form-control', 'id'=>'input-icon2'));
				?>
			</div>
			<?php
			echo form_button(array('type' => 'submit', 'content' => '<b>Entrar <i class="fa fa-sign-in" aria-hidden="true"></i></b>', 'class' => 'btn btn-success btn-block')) .
				form_close();

			?>
		</div>
		<?php
		if (validation_errors()) {
			echo "<div id='errors' class=\"alert alert-danger\">" . validation_errors() . "</div>";
		}
		?>
	</div>
</div>

